import Vue from 'vue'
import VueResource from 'vue-resource'
import App from './components/HomePage.vue'
import './plugins/element.js'
import apis from './components/apis'
import utils from './components/utils'

Vue.use(VueResource);
Vue.config.productionTip = false
Vue.prototype.apis = apis
Vue.prototype.utils = utils

new Vue({
  render: h => h(App),
}).$mount('#app')
