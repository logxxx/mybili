
function IsInteger(num) {
    if (!isNaN(num) && num % 1 === 0) {
        return true;
    } else {
        return false;
    }
}

function FormatTime(t) {
    if(!t) {
        return "-"
    }
    var date = new Date(t*1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
    var year = date.getFullYear(),
        month = ("0" + (date.getMonth() + 1)).slice(-2),
        sdate = ("0" + date.getDate()).slice(-2),
        hour = ("0" + date.getHours()).slice(-2),
        minute = ("0" + date.getMinutes()).slice(-2),
        second = ("0" + date.getSeconds()).slice(-2);
    // 拼接
    var result = year + "-"+ month +"-"+ sdate +" "+ hour +":"+ minute +":" + second;
    // 返回
    return result;

}

function BeautySub(str, len) {
    var reg = /[\u4e00-\u9fa5]/g,slice = str.substring(0, len),
        cCharNum = (~~(slice.match(reg) && slice.match(reg).length)),
        realen = slice.length*2 - cCharNum-1;
    return str.substr(0, realen) + (realen < str.length ? "..." : "");
}

function ConvertSize(size) {

    if(!size){
        return 0+"b"
    }

    let sizeUnit = "b"
    if(size > 1024){
        size /= 1024
        sizeUnit = "kb"
    }
    if(size>1024) {
        size /= 1024
        sizeUnit = "MB"
    }
    if(size>1024) {
        size /= 1024
        sizeUnit = "GB"
    }

    return size.toFixed(2) + sizeUnit
}

export default {
    IsInteger,
    BeautySub,
    ConvertSize,
    FormatTime
}