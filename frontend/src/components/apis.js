
function Ping(that, fn) {
    that.$http.get("ping").then(function(resp){
        //console.log("GetDownloadList resp:", resp)
        fn(resp.body)
    })
}

function UserAdvise(that, req, fn) {
    that.$http.post("user/advise", req).then(function(resp){
        //console.log("GetDownloadList resp:", resp)
        fn(resp.body)
    })
}

function GetDownloadList(that, req, fn) {
    //console.log("GetDownloadList start")
    that.$http.get("download/list?"+req).then(function(resp){
        //console.log("GetDownloadList resp:", resp)
        fn(resp.body)
    })
}

function GetDeviceInfo(that, fn) {
    console.log("GetDeviceInfo start")
    that.$http.get("device/info").then(function(resp){
        console.log("GetDeviceInfo resp:", resp.data)
        fn(resp.data)
    })
}

function Download(that, req, fn) {
    console.log("Download start")
    that.$http.post("download",req).then(function(resp){
        console.log("Download resp:", resp)
        fn(resp.data)
    })
}

function RetryDownload(that, req, fn) {
    console.log("RetryDownload start")
    that.$http.post("download/retry",req).then(function(resp){
        console.log("Download resp:", resp)
        fn(resp.data)
    })
}

function DeleteDownload(that, req, fn) {
    console.log("RetryDownload start")
    that.$http.post("download/delete",req).then(function(resp){
        console.log("Download resp:", resp)
        fn(resp.data)
    })
}

export default {
    GetDeviceInfo,
    GetDownloadList,
    Download,
    RetryDownload,
    DeleteDownload,
    UserAdvise,
    Ping,
}