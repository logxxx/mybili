package main

import (
	"github.com/logxxx/mybili_pkg/consts"
	"github.com/logxxx/mybili_pkg/utils/log"
	"github.com/logxxx/mybili_pkg/utils/runutil"
	"logxxx.com/mybili/globalconfig"
	"logxxx.com/mybili/modules/download/downloadbiz"
	"logxxx.com/mybili/modules/download/downloaddao"
	"logxxx.com/mybili/modules/download/downloadweb"
	"logxxx.com/mybili/modules/downloader"
	"logxxx.com/mybili/modules/report"
	"os"
	"path/filepath"
)

func main() {

	wd, _ := os.Getwd()
	log.Infof("main wd:%v", wd)
	dbPath := filepath.Join(wd, "db")

	globalconfig.MustInit()

	downloader.Init()

	err := downloaddao.Init(dbPath)
	if err != nil {
		panic(err)
	}

	downloadbiz.Init()

	go downloadweb.Init()

	reportEvent(consts.ActionStartup)

	runutil.WaitForExit(func() {
		reportEvent(consts.ActionShutdown)
	})

}

func reportEvent(action string) {
	req := &report.ReportReq{
		Action: action,
	}
	err := report.Report(req)
	if err != nil {
		log.Errorf("exit Report err:%v", err)
	}
}
