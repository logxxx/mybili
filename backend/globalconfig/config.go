package globalconfig

import (
	"embed"
	"github.com/logxxx/mybili_pkg/utils/log"
	"gopkg.in/yaml.v2"
	"logxxx.com/mybili/modules/basecfg"
	"logxxx.com/mybili/modules/download/downloadcfg"
	"logxxx.com/mybili/modules/report/reportcfg"
)

var (
	_cfg *GlobalConfig
)

//go:embed config/config.yaml
var configFS embed.FS

type GlobalConfig struct {
	BaseCfg     *basecfg.BaseConfig         `json:"base" yaml:"base"`
	DownloadCfg *downloadcfg.DownloadConfig `json:"download" yaml:"download"`
	ReportCfg   *reportcfg.ReportConfig     `json:"report" yaml:"report"`
}

func MustInit() {

	configBytes, err := configFS.ReadFile("config/config.yaml")
	if err != nil {
		panic(err)
	}

	cfg := &GlobalConfig{}
	err = yaml.Unmarshal(configBytes, cfg)
	if err != nil {
		panic(err)
	}
	_cfg = cfg

	log.Infof("global cfg init succ:%+v", log.JSONGrace(_cfg))

	basecfg.SetConfig(_cfg.BaseCfg)
	downloadcfg.SetConfig(_cfg.DownloadCfg)
	reportcfg.SetConfig(_cfg.ReportCfg)

}
