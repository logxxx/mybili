package main

import (
	"github.com/logxxx/mybili_pkg/utils/log"
	"github.com/logxxx/utils/fileutil"
	"logxxx.com/mybili/biliutils"
	"math/rand"
	"os"
	"strings"
)

var (
	_cookie = ""
)

type GetFollowsResp struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	TTL     int    `json:"ttl"`
	Data    struct {
		List []struct {
			Mid          int64       `json:"mid"`
			Attribute    int         `json:"attribute"`
			Mtime        int         `json:"mtime"`
			Tag          interface{} `json:"tag"`
			Special      int         `json:"special"`
			ContractInfo struct {
			} `json:"contract_info"`
			Uname          string `json:"uname"`
			Face           string `json:"face"`
			Sign           string `json:"sign"`
			FaceNft        int    `json:"face_nft"`
			OfficialVerify struct {
				Type int    `json:"type"`
				Desc string `json:"desc"`
			} `json:"official_verify"`
			Vip struct {
				VipType       int    `json:"vipType"`
				VipDueDate    int    `json:"vipDueDate"`
				DueRemark     string `json:"dueRemark"`
				AccessStatus  int    `json:"accessStatus"`
				VipStatus     int    `json:"vipStatus"`
				VipStatusWarn string `json:"vipStatusWarn"`
				ThemeType     int    `json:"themeType"`
				Label         struct {
					Path        string `json:"path"`
					Text        string `json:"text"`
					LabelTheme  string `json:"label_theme"`
					TextColor   string `json:"text_color"`
					BgStyle     int    `json:"bg_style"`
					BgColor     string `json:"bg_color"`
					BorderColor string `json:"border_color"`
				} `json:"label"`
				AvatarSubscript    int    `json:"avatar_subscript"`
				NicknameColor      string `json:"nickname_color"`
				AvatarSubscriptURL string `json:"avatar_subscript_url"`
			} `json:"vip"`
			NameRender struct {
			} `json:"name_render"`
			NftIcon   string `json:"nft_icon"`
			RecReason string `json:"rec_reason"`
			TrackID   string `json:"track_id"`
		} `json:"list"`
		ReVersion int `json:"re_version"`
		Total     int `json:"total"`
	} `json:"data"`
}

//判断用户是否被封:
//https://api.bilibili.com/x/space/wbi/acc/info?mid=3546646908504837&token=&platform=web&web_location=1550101&dm_img_list=[]&dm_img_str=V2ViR0wgMS4wIChPcGVuR0wgRVMgMi4wIENocm9taXVtKQ&dm_cover_img_str=QU5HTEUgKEludGVsLCBJbnRlbChSKSBVSEQgR3JhcGhpY3MgNjMwICgweDAwMDAzRTkyKSBEaXJlY3QzRDExIHZzXzVfMCBwc181XzAsIEQzRDExKUdvb2dsZSBJbmMuIChJbnRlbC&dm_img_inter=%7B%22ds%22:[],%22wh%22:[2630,135,106],%22of%22:[471,942,471]%7D
//resp.slience=1

var (
	skipUidMap = map[int64]bool{
		4925207: true,
		8615846: true,
	}
	skipUnameMap = map[string]bool{
		"芙芙家的洗碗君": true,
		"话漫人儿":    true,
	}
)

func main() {

	os.Chdir("design/01extract_follow")

	follows := []GetFollowsResp{}
	err := fileutil.ReadJsonFile("follow.txt", &follows)
	if err != nil {
		panic(err)
	}
	rand.Shuffle(len(follows), func(i, j int) {
		follows[i], follows[j] = follows[j], follows[i]
	})
	log.Printf("get %v follows", len(follows))

	count := 0
	resp := []biliutils.Uper{}
	uniq := map[int64]bool{}
	for _, elem := range follows {
		for _, f := range elem.Data.List {

			if skipUidMap[f.Mid] || skipUnameMap[f.Uname] {
				continue
			}

			if uniq[f.Mid] {
				continue
			}
			uniq[f.Mid] = true
			if strings.Contains(f.Uname, "账号已注销") {
				continue
			}
			if f.Mid == 1091840904 {
				log.Printf("[%v]%+v", count, f)
			}
			log.Printf("[%v]%+v", count, f)
			resp = append(resp, biliutils.Uper{
				Uid:  f.Mid,
				Name: f.Uname,
			})
			count++
			//log.Printf("[%v]%v %v", count, f.Mid, f.Uname)
		}
	}

	fileutil.WriteJsonToFile(resp, "extract.json")

}
