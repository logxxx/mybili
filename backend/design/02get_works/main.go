package main

import (
	"github.com/logxxx/utils"
	"github.com/logxxx/utils/fileutil"
	"log"
	"logxxx.com/mybili/biliutils"
	"logxxx.com/mybili/modules/download/downloadbiz"
	"math/rand"
	"os"
	"path/filepath"
	"time"
)

var (
	skipUidMap = map[int64]bool{
		4925207:    true,
		8615846:    true,
		2069659131: true,
	}
	skipUnameMap = map[string]bool{
		"芙芙家的洗碗君": true,
		"话漫人儿":    true,
		"英雄联盟a小孟": true,
	}
)

func main() {

	os.Chdir("design/02get_works")

	upers := []biliutils.Uper{}

	fileutil.ReadJsonFile("chore/followers.json", &upers)

	rand.Shuffle(len(upers), func(i, j int) {
		upers[i], upers[j] = upers[j], upers[i]
	})

	for i, uper := range upers {

		if skipUidMap[uper.Uid] || skipUnameMap[uper.Name] {
			continue
		}

		time.Sleep(5 * time.Second)
		log.Printf("run uper %v/%v:%+v", i+1, len(upers), uper)
		works, err := biliutils.GetUperAllWorks(uper.Uid)
		if err != nil {
			log.Printf("GetUperAllWorks err:%v uid:%v", err, uper.Uid)
			continue
		}
		log.Printf("get %v works:", works)
		for i := range works {
			if IsDownloaded(works[i].Bvid) {
				continue
			}
			log.Printf("download %v/%v: %+v", i+1, len(works), works[i])
			downloadbiz.DownloadByBvidByBBDown(works[i].Bvid)
			SetDownloaded(works[i])
		}
	}

}

func SetDownloaded(work biliutils.Work) {
	filePath := filepath.Join("chore", "downloaded", work.Bvid)
	fileutil.WriteJsonToFile(work, filePath)
}

func IsDownloaded(bvid string) bool {
	return utils.HasFile(filepath.Join("chore", "downloaded", bvid))
}
