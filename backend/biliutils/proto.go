package biliutils

type Uper struct {
	Uid  int64  `json:"uid,omitempty"`
	Name string `json:"name,omitempty"`
}
