package biliutils

import (
	"encoding/json"
	"fmt"
	"github.com/tidwall/gjson"
	"io"
	"net/http"
	"time"
)

var (
	_cookie = `buvid3=4F1D39C7-88A7-3361-760E-D5C454365E6935022infoc; b_nut=1715152735; _uuid=1010FE9AEF-2DD8-32B5-9E54-83AAFEA29EEB34042infoc; rpdid=|(J|~|Ru)~uR0J'u~ululk~JY; header_theme_version=CLOSE; iflogin_when_web_push=1; bp_t_offset_261168106=931758700641648664; enable_web_push=DISABLE; DedeUserID=1120549278; DedeUserID__ckMd5=41f8d0dae0095e18; buvid4=CDD7D4CF-E8CF-BF8D-81DE-A9629BE9722F31013-022042916-aqrJQQc0ovEXjjPAs8PP8Qg%2FKd%2BOyDIn025ojCmRffAyw2L5%2BC6jAw%3D%3D; buvid_fp_plain=undefined; PVID=5; fingerprint=38374f1f6a88ea42d418bb3141cffc19; buvid_fp=38374f1f6a88ea42d418bb3141cffc19; CURRENT_FNVAL=4048; bili_ticket=eyJhbGciOiJIUzI1NiIsImtpZCI6InMwMyIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTgzNzI1NDgsImlhdCI6MTcxODExMzI4OCwicGx0IjotMX0.6f-hywFzfG-YerpXW8VrC5zdk85i39xZ7Id8UDBeJik; bili_ticket_expires=1718372488; SESSDATA=303cadc4%2C1733665359%2C112cd%2A62CjAX62RZNlpjtFB2KjEDKcZHWVbO4YNf4uWCKaMybY97aJhZIBdisVSNAX_qOBsYNN0SVjRqaGR5dWNCTHhmMVJucno2U0M1SE0xLU5RNzdaMXVwV2NPVGF4RUJHVFVVQmFzUnhKQkFxX25yb0NtdFowLW42QzhvOWdkMjJCM1FEWmtqemhialNBIIEC; bili_jct=71f8cb69bec1062c8e5ce2a70d027236; sid=7np5rahr; bp_t_offset_1120549278=942142913690730501; b_lsid=EB4FB1E4_190115DEB24; home_feed_column=5; browser_resolution=1920-919`
)

func SetCookie(cookie string) {
	_cookie = cookie
}

func IsUperBanned(mid int64) (resp bool) {
	urlStr := fmt.Sprintf("https://api.bilibili.com/x/space/wbi/acc/info?mid=%v&token=&platform=web&web_location=1550101&dm_img_list=[]&dm_img_str=V2ViR0wgMS4wIChPcGVuR0wgRVMgMi4wIENocm9taXVtKQ&dm_cover_img_str=QU5HTEUgKEludGVsLCBJbnRlbChSKSBVSEQgR3JhcGhpY3MgNjMwICgweDAwMDAzRTkyKSBEaXJlY3QzRDExIHZzXzVfMCBwc181XzAsIEQzRDExKUdvb2dsZSBJbmMuIChJbnRlbC&dm_img_inter=%7B%22ds%22:[],%22wh%22:[2630,135,106],%22of%22:[471,942,471]%7D", mid)
	newUrlStr, err := SignAndGenerateURL(urlStr)
	if err != nil {
		fmt.Printf("Error: %s", err)
		return
	}

	req, err := http.NewRequest("GET", newUrlStr, nil)
	if err != nil {
		fmt.Printf("Error: %s", err)
		return
	}
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36")
	req.Header.Set("Referer", "https://www.bilibili.com/")
	response, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Printf("Request failed: %s", err)
		return
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		fmt.Printf("Failed to read response: %s", err)
		return
	}
	fmt.Println("resp:", string(body))
	return gjson.Get(string(body), "data.silence").Int() == 1

}

type GetUperWorksResp struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	TTL     int    `json:"ttl"`
	Data    struct {
		List struct {
			Tlist interface{} `json:"tlist"`
			Vlist []struct {
				Comment          int         `json:"comment"`
				Typeid           int         `json:"typeid"`
				Play             int         `json:"play"`
				Pic              string      `json:"pic"`
				Subtitle         string      `json:"subtitle"`
				Description      string      `json:"description"`
				Copyright        string      `json:"copyright"`
				Title            string      `json:"title"`
				Review           int         `json:"review"`
				Author           string      `json:"author"`
				Mid              int64       `json:"mid"`
				Created          int         `json:"created"`
				Length           string      `json:"length"`
				VideoReview      int         `json:"video_review"`
				Aid              int         `json:"aid"`
				Bvid             string      `json:"bvid"`
				HideClick        bool        `json:"hide_click"`
				IsPay            int         `json:"is_pay"`
				IsUnionVideo     int         `json:"is_union_video"`
				IsSteinsGate     int         `json:"is_steins_gate"`
				IsLivePlayback   int         `json:"is_live_playback"`
				IsLessonVideo    int         `json:"is_lesson_video"`
				IsLessonFinished int         `json:"is_lesson_finished"`
				LessonUpdateInfo string      `json:"lesson_update_info"`
				JumpURL          string      `json:"jump_url"`
				Meta             interface{} `json:"meta"`
				IsAvoided        int         `json:"is_avoided"`
				SeasonID         int         `json:"season_id"`
				Attribute        int         `json:"attribute"`
				IsChargingArc    bool        `json:"is_charging_arc"`
				Vt               int         `json:"vt"`
				EnableVt         int         `json:"enable_vt"`
				VtDisplay        string      `json:"vt_display"`
				PlaybackPosition int         `json:"playback_position"`
			} `json:"vlist"`
			Slist []struct {
				Count       int    `json:"count"`
				Name        string `json:"name"`
				SpecialType string `json:"special_type"`
			} `json:"slist"`
		} `json:"list"`
		Page struct {
			Pn    int `json:"pn"`
			Ps    int `json:"ps"`
			Count int `json:"count"`
		} `json:"page"`
		EpisodicButton struct {
			Text string `json:"text"`
			URI  string `json:"uri"`
		} `json:"episodic_button"`
		IsRisk      bool        `json:"is_risk"`
		GaiaResType int         `json:"gaia_res_type"`
		GaiaData    interface{} `json:"gaia_data"`
	} `json:"data"`
}

type Work struct {
	UperMid  int64
	UperName string
	Bvid     string
	Title    string
}

func GetUperAllWorks(mid int64) (resp []Work, err error) {
	for i := 1; i < 100; i++ {
		works := []Work{}
		works, err = GetUperWorks(mid, i)
		if err != nil {
			return
		}
		resp = append(resp, works...)
		if len(works) < 50 {
			break
		}
		time.Sleep(5 * time.Second)
	}
	return resp, nil
}

func GetUperWorks(mid int64, page int) (resp []Work, err error) {

	reqURL := fmt.Sprintf("https://api.bilibili.com/x/space/wbi/arc/search?mid=%v&pn=%v&ps=50&index=1&order=pubdate&order_avoided=true&platform=web&web_location=1550101&dm_img_list=[%7B%22x%22:736,%22y%22:851,%22z%22:0,%22timestamp%22:2,%22k%22:110,%22type%22:0%7D]&dm_img_str=V2ViR0wgMS4wIChPcGVuR0wgRVMgMi4wIENocm9taXVtKQ&dm_cover_img_str=QU5HTEUgKEludGVsLCBJbnRlbChSKSBVSEQgR3JhcGhpY3MgNjMwICgweDAwMDAzRTkyKSBEaXJlY3QzRDExIHZzXzVfMCBwc181XzAsIEQzRDExKUdvb2dsZSBJbmMuIChJbnRlbC&dm_img_inter=%7B%22ds%22:[%7B%22t%22:0,%22c%22:%22ZXllLXByb3RlY3Rvci1wcm9jZXNzZW%22,%22p%22:[168,56,56],%22s%22:[58,2543,-2584]%7D],%22wh%22:[2497,114,57],%22of%22:[339,678,339]%7D", mid, page)

	reqURL, err = SignAndGenerateURL(reqURL)
	if err != nil {
		return nil, err
	}

	//log.Printf("reqURL:%v", reqURL)

	req, err := http.NewRequest("GET", reqURL, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Accept", "*/*")
	req.Header.Set("Accept-Language", "zh-CN,zh;q=0.9")
	req.Header.Set("Cache-Control", "no-cache")
	req.Header.Set("Cookie", _cookie)
	req.Header.Set("Origin", "https://space.bilibili.com")
	req.Header.Set("Pragma", "no-cache")
	req.Header.Set("Priority", "u=1, i")
	req.Header.Set("Referer", "https://space.bilibili.com/3546646908504837/video")
	req.Header.Set("Sec-Ch-Ua", "\"Google Chrome\";v=\"125\", \"Chromium\";v=\"125\", \"Not.A/Brand\";v=\"24\"")
	req.Header.Set("Sec-Ch-Ua-Mobile", "?0")
	req.Header.Set("Sec-Ch-Ua-Platform", "\"Windows\"")
	req.Header.Set("Sec-Fetch-Dest", "empty")
	req.Header.Set("Sec-Fetch-Mode", "cors")
	req.Header.Set("Sec-Fetch-Site", "same-site")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.36")

	httpResp, err := http.DefaultClient.Do(req)
	if err != nil {
		return
	}
	defer httpResp.Body.Close()

	respBytes, err := io.ReadAll(httpResp.Body)
	if err != nil {
		return
	}
	//log.Printf("resp:%v", string(respBytes))

	worksResp := &GetUperWorksResp{}

	json.Unmarshal(respBytes, worksResp)

	works := []Work{}

	for _, v := range worksResp.Data.List.Vlist {
		works = append(works, Work{
			UperMid:  v.Mid,
			UperName: v.Author,
			Bvid:     v.Bvid,
			Title:    v.Title,
		})
	}

	return works, nil
}
