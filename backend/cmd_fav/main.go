package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"logxxx.com/mybili/modules/download/downloadbiz"
	"net/http"
	"net/url"
	"strings"
)

func main() {
	mediaID := ""
	cookie := ""

	downloadbiz.SetCookie(cookie)
	_, err := DownloadFav(mediaID)
	if err != nil {
		panic(err)
	}
}

func CancelFav() {

}

func CancelFavRaw() {
	// Generated by curl-to-Go: https://mholt.github.io/curl-to-go

	// curl 'https://api.bilibili.com/x/v3/fav/resource/deal' \
	//   -H 'accept: application/json, text/plain, */*' \
	//   -H 'accept-language: zh-CN,zh;q=0.9' \
	//   -H 'cache-control: no-cache' \
	//   -H 'content-type: application/x-www-form-urlencoded' \
	//   -H $'cookie: buvid3=4F1D39C7-88A7-3361-760E-D5C454365E6935022infoc; b_nut=1715152735; _uuid=1010FE9AEF-2DD8-32B5-9E54-83AAFEA29EEB34042infoc; rpdid=|(J|~|Ru)~uR0J\'u~ululk~JY; header_theme_version=CLOSE; iflogin_when_web_push=1; bp_t_offset_261168106=931758700641648664; enable_web_push=DISABLE; DedeUserID=1120549278; DedeUserID__ckMd5=41f8d0dae0095e18; buvid4=CDD7D4CF-E8CF-BF8D-81DE-A9629BE9722F31013-022042916-aqrJQQc0ovEXjjPAs8PP8Qg%2FKd%2BOyDIn025ojCmRffAyw2L5%2BC6jAw%3D%3D; buvid_fp_plain=undefined; PVID=5; bp_t_offset_1120549278=932106507124736023; fingerprint=38374f1f6a88ea42d418bb3141cffc19; buvid_fp=38374f1f6a88ea42d418bb3141cffc19; bsource=search_baidu; SESSDATA=fdeb48b8%2C1732432437%2C59e79%2A52CjA70obcle7ue3mgTcNQgHNcY1prD8ePBwu-rMEdYBFxPU8gXwu5Q2g7Ec4Ir8Sy5OASVkRrQ0thbExLUFEzeU5qanZKV0dLMWFOVkhaT2F3TzFZclNQM1l2dnNwM2FXRms0NUFDYzFZSWtBaUI5MWtwYWFaTWFWaFRYekgwV3V2V0QzQ19fOE9RIIEC; bili_jct=22fa6c0c821fb85dd59a07d9db0f0f37; bili_ticket=eyJhbGciOiJIUzI1NiIsImtpZCI6InMwMyIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTcxMzk2MzksImlhdCI6MTcxNjg4MDM3OSwicGx0IjotMX0.1PWWWuJVLY0gk2LCAmv22CCRI-zZ2h-9_g8yVkJ7CLg; bili_ticket_expires=1717139579; b_lsid=18CC5643_18FBF4D12DC; home_feed_column=4; browser_resolution=1060-1886; CURRENT_FNVAL=4048; sid=7edjfy56' \
	//   -H 'origin: https://www.bilibili.com' \
	//   -H 'pragma: no-cache' \
	//   -H 'priority: u=1, i' \
	//   -H 'referer: https://www.bilibili.com/video/BV1Ti421D7uT/?spm_id_from=333.1007.tianma.1-3-3.click&vd_source=83be85988224ac4f7fc490cbc6000946' \
	//   -H 'sec-ch-ua: "Google Chrome";v="125", "Chromium";v="125", "Not.A/Brand";v="24"' \
	//   -H 'sec-ch-ua-mobile: ?0' \
	//   -H 'sec-ch-ua-platform: "Windows"' \
	//   -H 'sec-fetch-dest: empty' \
	//   -H 'sec-fetch-mode: cors' \
	//   -H 'sec-fetch-site: same-site' \
	//   -H 'user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.36' \
	//   --data-raw 'rid=1454668638&type=2&add_media_ids=&del_media_ids=1591924378&platform=web&eab_x=2&ramval=18&ga=1&gaia_source=web_normal&csrf=22fa6c0c821fb85dd59a07d9db0f0f37'

	params := url.Values{}
	params.Add("rid", `1454668638`)
	params.Add("type", `2`)
	params.Add("add_media_ids", ``)
	params.Add("del_media_ids", `1591924378`)
	params.Add("platform", `web`)
	params.Add("eab_x", `2`)
	params.Add("ramval", `18`)
	params.Add("ga", `1`)
	params.Add("gaia_source", `web_normal`)
	params.Add("csrf", `22fa6c0c821fb85dd59a07d9db0f0f37`)
	body := strings.NewReader(params.Encode())

	req, err := http.NewRequest("POST", "https://api.bilibili.com/x/v3/fav/resource/deal", body)
	if err != nil {
		// handle err
	}
	req.Header.Set("Accept", "application/json, text/plain, */*")
	req.Header.Set("Accept-Language", "zh-CN,zh;q=0.9")
	req.Header.Set("Cache-Control", "no-cache")
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Cookie", "buvid3=4F1D39C7-88A7-3361-760E-D5C454365E6935022infoc; b_nut=1715152735; _uuid=1010FE9AEF-2DD8-32B5-9E54-83AAFEA29EEB34042infoc; rpdid=|(J|~|Ru)~uR0J\\'u~ululk~JY; header_theme_version=CLOSE; iflogin_when_web_push=1; bp_t_offset_261168106=931758700641648664; enable_web_push=DISABLE; DedeUserID=1120549278; DedeUserID__ckMd5=41f8d0dae0095e18; buvid4=CDD7D4CF-E8CF-BF8D-81DE-A9629BE9722F31013-022042916-aqrJQQc0ovEXjjPAs8PP8Qg%2FKd%2BOyDIn025ojCmRffAyw2L5%2BC6jAw%3D%3D; buvid_fp_plain=undefined; PVID=5; bp_t_offset_1120549278=932106507124736023; fingerprint=38374f1f6a88ea42d418bb3141cffc19; buvid_fp=38374f1f6a88ea42d418bb3141cffc19; bsource=search_baidu; SESSDATA=fdeb48b8%2C1732432437%2C59e79%2A52CjA70obcle7ue3mgTcNQgHNcY1prD8ePBwu-rMEdYBFxPU8gXwu5Q2g7Ec4Ir8Sy5OASVkRrQ0thbExLUFEzeU5qanZKV0dLMWFOVkhaT2F3TzFZclNQM1l2dnNwM2FXRms0NUFDYzFZSWtBaUI5MWtwYWFaTWFWaFRYekgwV3V2V0QzQ19fOE9RIIEC; bili_jct=22fa6c0c821fb85dd59a07d9db0f0f37; bili_ticket=eyJhbGciOiJIUzI1NiIsImtpZCI6InMwMyIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTcxMzk2MzksImlhdCI6MTcxNjg4MDM3OSwicGx0IjotMX0.1PWWWuJVLY0gk2LCAmv22CCRI-zZ2h-9_g8yVkJ7CLg; bili_ticket_expires=1717139579; b_lsid=18CC5643_18FBF4D12DC; home_feed_column=4; browser_resolution=1060-1886; CURRENT_FNVAL=4048; sid=7edjfy56")
	req.Header.Set("Origin", "https://www.bilibili.com")
	req.Header.Set("Pragma", "no-cache")
	req.Header.Set("Priority", "u=1, i")
	req.Header.Set("Referer", "https://www.bilibili.com/video/BV1Ti421D7uT/?spm_id_from=333.1007.tianma.1-3-3.click&vd_source=83be85988224ac4f7fc490cbc6000946")
	req.Header.Set("Sec-Ch-Ua", "\"Google Chrome\";v=\"125\", \"Chromium\";v=\"125\", \"Not.A/Brand\";v=\"24\"")
	req.Header.Set("Sec-Ch-Ua-Mobile", "?0")
	req.Header.Set("Sec-Ch-Ua-Platform", "\"Windows\"")
	req.Header.Set("Sec-Fetch-Dest", "empty")
	req.Header.Set("Sec-Fetch-Mode", "cors")
	req.Header.Set("Sec-Fetch-Site", "same-site")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.36")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		// handle err
	}
	defer resp.Body.Close()
}

var found = false

var targetID = ""

func DownloadFav(mediaID string) (result interface{}, err error) {

	for page := 1; page < 100; page++ {

		bvids := []string{}
		bvids, err = GetFav(mediaID, page)
		if err != nil {
			return
		}
		if len(bvids) <= 0 {
			break
		}
		for i, id := range bvids {

			if id == targetID {
				found = true
			}
			if targetID != "" && !found {
				continue
			}

			//playLink := fmt.Sprintf("https://www.bilibili.com/video/%v", id)
			//log.Printf("id%v:%v", i+1, playLink)
			log.Printf("id%v: %v", i+1, id)
			downloadbiz.DownloadByBvidByBBDown(id)
		}
	}

	return
}

func GetFav(mediaID string, page int) (bvids []string, err error) {
	//大号
	reqURL := fmt.Sprintf("https://api.bilibili.com/x/v3/fav/resource/list?media_id=%v&pn=%v&ps=20&keyword=&order=mtime&type=0&tid=0&platform=web", mediaID, page)
	//小号
	//reqURL := fmt.Sprintf("https://api.bilibili.com/x/v3/fav/resource/list?media_id=1591924378&pn=%v&ps=20&keyword=&order=mtime&type=0&tid=0&platform=web", page)
	resp := &FavResp{}

	httpResp, err := http.Get(reqURL)
	if err != nil {
		return
	}

	if httpResp.StatusCode != 200 {
		err = fmt.Errorf("invalid code:%v", httpResp.StatusCode)
		return
	}

	respBytes, err := ioutil.ReadAll(httpResp.Body)
	if err != nil {
		return
	}

	//log.Printf("GetFav(page=%v) resp:%v", page, string(respBytes))

	err = json.Unmarshal(respBytes, resp)
	if err != nil {
		return
	}
	for i, info := range resp.Data.Medias {
		log.Printf("page=%v idx=%v info=%+v", page, i+1, info)
		if info.Page > 10 {
			continue
		}
		bvids = append(bvids, info.Bvid)
	}

	return
}

type FavResp struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	TTL     int    `json:"ttl"`
	Data    struct {
		Info struct {
			ID    int    `json:"id"`
			Fid   int    `json:"fid"`
			Mid   int    `json:"mid"`
			Attr  int    `json:"attr"`
			Title string `json:"title"`
			Cover string `json:"cover"`
			Upper struct {
				Mid       int    `json:"mid"`
				Name      string `json:"name"`
				Face      string `json:"face"`
				Followed  bool   `json:"followed"`
				VipType   int    `json:"vip_type"`
				VipStatue int    `json:"vip_statue"`
			} `json:"upper"`
			CoverType int `json:"cover_type"`
			CntInfo   struct {
				Collect int `json:"collect"`
				Play    int `json:"play"`
				ThumbUp int `json:"thumb_up"`
				Share   int `json:"share"`
			} `json:"cnt_info"`
			Type       int    `json:"type"`
			Intro      string `json:"intro"`
			Ctime      int    `json:"ctime"`
			Mtime      int    `json:"mtime"`
			State      int    `json:"state"`
			FavState   int    `json:"fav_state"`
			LikeState  int    `json:"like_state"`
			MediaCount int    `json:"media_count"`
		} `json:"info"`
		Medias []struct {
			ID       int    `json:"id"`
			Type     int    `json:"type"`
			Title    string `json:"title"`
			Cover    string `json:"cover"`
			Intro    string `json:"intro"`
			Page     int    `json:"page"`
			Duration int    `json:"duration"`
			Upper    struct {
				Mid  int64  `json:"mid"`
				Name string `json:"name"`
				Face string `json:"face"`
			} `json:"upper"`
			Attr    int `json:"attr"`
			CntInfo struct {
				Collect    int    `json:"collect"`
				Play       int    `json:"play"`
				Danmaku    int    `json:"danmaku"`
				Vt         int    `json:"vt"`
				PlaySwitch int    `json:"play_switch"`
				Reply      int    `json:"reply"`
				ViewText1  string `json:"view_text_1"`
			} `json:"cnt_info"`
			Link    string `json:"link"`
			Ctime   int    `json:"ctime"`
			Pubtime int    `json:"pubtime"`
			FavTime int    `json:"fav_time"`
			BvID    string `json:"bv_id"`
			Bvid    string `json:"bvid"`
			Ugc     struct {
				FirstCid int `json:"first_cid"`
			} `json:"ugc"`
		} `json:"medias"`
		HasMore bool `json:"has_more"`
		TTL     int  `json:"ttl"`
	} `json:"data"`
}
