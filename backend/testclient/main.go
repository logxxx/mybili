package main

import (
	"bytes"
	"encoding/json"
	"github.com/logxxx/mybili_pkg/utils/log"
	"io"
	"io/ioutil"
	"logxxx.com/mybili/modules/download/downloadproto"
	"net/http"
)

func MakeReq(obj interface{}) io.Reader {
	objJson, _ := json.Marshal(obj)
	return bytes.NewBuffer(objJson)
}

func main() {
	bvids := []string{"BV19F411G7wu", "BV1pv4y1P7tJ", "BV1hY4y1V7TK"}

	for _, bvid := range bvids {
		req := downloadproto.CreateDownloadReq{
			Bvid: bvid,
		}
		reqReader := MakeReq(req)
		resp, err := http.Post("http://localhost:8080/download", "application/json", reqReader)
		if err != nil {
			log.Errorf("err:%v", err)
			return
		}
		defer resp.Body.Close()
		respBytes, _ := ioutil.ReadAll(resp.Body)
		log.Infof("resp:%v", string(respBytes))
	}

}
