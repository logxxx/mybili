module logxxx.com/mybili

go 1.16

require (
	github.com/asdine/storm/v3 v3.2.1
	github.com/gin-gonic/gin v1.8.2
	github.com/golang/protobuf v1.5.2
	github.com/logxxx/mybili_pkg v1.0.17
	github.com/logxxx/utils v1.0.83
	github.com/stretchr/testify v1.8.1
	github.com/tidwall/gjson v1.17.1
	golang.org/x/mod v0.7.0
	golang.org/x/sys v0.4.0
	google.golang.org/grpc v1.47.0
	google.golang.org/protobuf v1.28.1
	gopkg.in/yaml.v2 v2.4.0
)
