package downloaddao_test

import (
	"logxxx.com/mybili/modules/download/downloadconst"
	"logxxx.com/mybili/modules/download/downloaddao"
	"logxxx.com/mybili/modules/download/downloadproto"
	"os"
	"path/filepath"
	"testing"
)

func TestNewDownloadDao(t *testing.T) {

	wd, _ := os.Getwd()
	t.Logf("wd:%v", wd)
	dbPath := filepath.Join(wd, "db")
	err := downloaddao.Init(dbPath)
	if err != nil {
		t.Fatal(err)
	}

	dao := downloaddao.GetDownloadDao()
	_, err = dao.GetTaskList(downloadconst.DownloadStatusRunning)
	if err != nil {
		t.Fatal(err)
	}

	req2 := make([]*downloadproto.VideoInfo, 0)
	req2 = append(req2, &downloadproto.VideoInfo{
		Bvid: "1",
		Avid: 1,
	})
	req2 = append(req2, &downloadproto.VideoInfo{
		Bvid: "2",
		Avid: 2,
	})
	err = dao.SetTaskList(downloadconst.DownloadStatusRunning, req2)
	if err != nil {
		t.Fatal(err)
	}

	resp3, err := dao.GetTaskList(downloadconst.DownloadStatusRunning)
	if err != nil {
		t.Fatal(err)
	}
	for i, task := range resp3 {
		t.Logf("%v: %+v", i, task)
	}

}
