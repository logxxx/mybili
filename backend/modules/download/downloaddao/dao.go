package downloaddao

import (
	"github.com/asdine/storm/v3"
	"logxxx.com/mybili/modules/download/downloadproto"
	"os"
	"path/filepath"
)

var (
	_dao *DownloadDao
)

type DownloadDao struct {
	taskDB *storm.DB
}

func NewDownloadDao(dbPath string) (*DownloadDao, error) {

	taskDbPath := filepath.Join(dbPath, "task.db")

	db, err := storm.Open(taskDbPath)
	if err != nil {
		return nil, err
	}

	dao := &DownloadDao{
		taskDB: db,
	}

	return dao, nil
}

func Init(dbPath string) error {

	_ = os.MkdirAll(dbPath, 0777)

	dao, err := NewDownloadDao(dbPath)
	if err != nil {
		return err
	}
	_dao = dao
	return nil
}

func GetDownloadDao() *DownloadDao {
	return _dao
}

func (dao *DownloadDao) SetTaskList(status string, tasks []*downloadproto.VideoInfo) error {
	return dao.taskDB.Set("tasks", status, tasks)
}

func (dao *DownloadDao) GetTaskList(status string) ([]*downloadproto.VideoInfo, error) {

	resp := make([]*downloadproto.VideoInfo, 0)
	err := dao.taskDB.Get("tasks", status, &resp)
	if err != nil {
		if err == storm.ErrNotFound {
			return nil, nil
		}
		return nil, err
	}

	return resp, nil

}
