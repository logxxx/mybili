package downloadproto

type CreateDownloadReq struct {
	Bvid      string `json:"bvid"`
	Links     string `json:"links"` //页面链接，用逗号分割
	Force     bool   `json:"force"`
	NeedDanmu bool   `json:"need_danmu"`
}

type DeleteDownloadReq struct {
	Bvid     string `json:"bvid"`
	Status   string `json:"status"`
	WithFile bool   `json:"with_file"`
}

type GetVideoInfoReq struct {
	Bvid string `json:"bvid"`
}

type GetVideoInfoResp struct {
	VideoInfo *VideoInfo `json:"video_info"`
}

type VideoInfo struct {
	Title        string     `json:"title"`
	Cover        string     `json:"cover"`
	Bvid         string     `json:"bvid"`
	Avid         int64      `json:"avid"`
	CidInfos     []*CidInfo `json:"cid_infos"`
	DownloadPath string     `json:"download_path"`
	FinishTime   int64      `json:"finish_time"`
	OwnerName    string     `json:"owner_name"`
}

type RetryDownloadReq struct {
	Bvid string `json:"bvid"`
	Cid  int64  `json:"cid"`
}

type GetDownloadListReq struct {
	Status string `json:"status"`
}

type GetDownloadListResp struct {
	Items     []*GetDownloadListItem `json:"items"`
	ExpireSec int64                  `json:"expire_sec"`
}

type GetDownloadListItem struct {
	Bvid         string                    `json:"bvid"`
	Avid         int64                     `json:"avid"`
	Title        string                    `json:"title"`
	Cover        string                    `json:"cover"` //封面
	Speed        int64                     `json:"speed"`
	TotalSize    int64                     `json:"total_size"`
	FinishSize   int64                     `json:"finish_size"`
	TimeLeftSec  int64                     `json:"time_left_sec"`
	DownloadPath string                    `json:"download_path"`
	FinishTime   int64                     `json:"finish_time"`
	ErrMsg       string                    `json:"err_msg"`
	SubItems     []*GetDownloadListSubItem `json:"sub_items"`
}

type GetDownloadListSubItem struct {
	Cid         int64  `json:"cid"`
	FileDir     string `json:"file_dir"`
	FileName    string `json:"file_name"`
	TotalSize   int64  `json:"total_size"`
	FinishSize  int64  `json:"finish_size"`
	Speed       int64  `json:"speed"`
	TimeLeftSec int64  `json:"time_left_sec"`
	ErrMsg      string `json:"err_msg"`
}
