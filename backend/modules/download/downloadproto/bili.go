package downloadproto

type ApiGetBidInfoResp struct {
	Code    int64            `json:"code"`
	Message string           `json:"message"`
	Data    ApiGetBidInfoBid `json:"data"`
}

type ApiGetBidInfoBid struct {
	Bvid     string              `json:"bvid"`
	Aid      int64               `json:"aid"`
	Videos   int64               `json:"videos"` //包含多少个视频
	Pic      string              `json:"pic"`    //封面
	Title    string              `json:"title"`
	Duration int64               `json:"duration"` //时长秒
	Owner    OwnerInfo           `json:"owner"`    //作者
	CidInfos []ApiGetBidInfoPage `json:"pages"`
}

type ApiGetBidInfoPage struct {
	Cid  int64  `json:"cid"`
	Page int64  `json:"page"`
	Part string `json:"part"`
}

type ApiGetPlayInfoResp struct {
	Durl []ApiGetPlayInfoDurl `json:"durl"`
}

type ApiGetPlayInfoDurl struct {
	Order     int64    `json:"order"`
	Length    int64    `json:"length"`
	Size      int64    `json:"size"`
	Url       string   `json:"url"`
	BackupUrl []string `json:"backup_url"`
}
