package downloadproto

type OwnerInfo struct {
	Name string `json:"name"`
	Face string `json:"face"`
}

type CidInfo struct {
	Aid       int64      `json:"aid"`
	Cid       int64      `json:"cid"`
	Page      int64      `json:"page"`
	Part      string     `json:"part"`
	RunTime   CidRuntime `json:"run_time"`
	OwnerName string
}

type CidRuntime struct {
	CreateTime int64  `json:"create_time"`
	UpdateTime int64  `json:"update_time"`
	StopTime   int64  `json:"stop_time"`
	TaskID     string `json:"task_id"`
	FileDir    string `json:"file_dir"`
	FileName   string `json:"file_name"`
	Speed      int64
	TotalSize  int64
	FinishSize int64
	ErrMsg     string
}

type DownloadVideoReq struct {
	Aid      int64  `json:"aid"`
	Cid      int64  `json:"cid"`
	Page     int64  `json:"page"`
	VideoUrl string `json:"video_url"`
	FileDir  string `json:"file_dir"`
	FileName string `json:"file_name"`
	Size     int64  `json:"size"`
}

type DownloadVideoResp struct {
	TaskID string
}

type DownloadState struct {
	StartTime  int64
	StopTime   int64
	Speed      int64
	TotalSize  int64
	FinishSize int64
	ErrMsg     string
}

type GetDeviceInfoResp struct {
	DownloadPath  string `json:"download_path"`
	DiskFreeSpace string `json:"disk_free_space"`
	ClientVersion string `json:"client_version"`
	StartupTime   int64  `json:"startup_time"`
	PosterUrl     string `json:"poster_url"`
}

type DownloadDanmuReq struct {
	Cid      int64
	Aid      int64
	FileDir  string `json:"file_dir"`
	FileName string `json:"file_name"`
}
