package downloadcontroller

import (
	"github.com/gin-gonic/gin"
	"github.com/logxxx/mybili_pkg/consts"
	"github.com/logxxx/mybili_pkg/proto"
	"github.com/logxxx/mybili_pkg/utils/log"
	"github.com/logxxx/mybili_pkg/utils/reqresp"
	"logxxx.com/mybili/modules/download/downloadbiz"
	"logxxx.com/mybili/modules/download/downloadproto"
	"logxxx.com/mybili/modules/report"
	"sync"
)

var (
	startup sync.Once
)

func UserAdvise(c *gin.Context) {

	req := &proto.UserAdviseRequest{}

	err := reqresp.ParseReq(c, req)
	if err != nil {
		reqresp.MakeErrMsg(c, err)
		return
	}

	log.Infof("收到了用户建议:%v", req.Content)

	reportReq := &report.ReportReq{
		Action:  consts.ActionUserAdvise,
		Payload: req,
	}
	err = report.Report(reportReq)
	if err != nil {
		reqresp.MakeErrMsg(c, err)
		return
	}

	reqresp.MakeRespOk(c)

}

func GetDeviceInfo(c *gin.Context) {

	resp, err := downloadbiz.GetDeviceInfo(c)
	if err != nil {
		reqresp.MakeErrMsg(c, err)
		return
	}

	reqresp.MakeResp(c, resp)

}

func GetVideoInfo(c *gin.Context) {
	req := &downloadproto.GetVideoInfoReq{}
	req.Bvid = c.Query("bvid")

	resp, err := downloadbiz.GetVideoInfo(c, req)
	if err != nil {
		reqresp.MakeErrMsg(c, err)
		return
	}

	reqresp.MakeResp(c, resp)
}

func DeleteTask(c *gin.Context) {
	req := &downloadproto.DeleteDownloadReq{}
	err := reqresp.ParseReq(c, req)
	if err != nil {
		reqresp.MakeErrMsg(c, err)
		return
	}

	err = downloadbiz.DeleteDownload(c, req)
	if err != nil {
		reqresp.MakeErrMsg(c, err)
		return
	}

	reqresp.MakeRespOk(c)
}

func Download(c *gin.Context) {

	req := &downloadproto.CreateDownloadReq{}
	err := reqresp.ParseReq(c, req)
	if err != nil {
		reqresp.MakeErrMsg(c, err)
		return
	}

	log.Infof("CreateDownload req:%v", log.JSONGrace(req))

	err = downloadbiz.CreateDownload(c, req)
	if err != nil {
		reqresp.MakeErrMsg(c, err)
		return
	}

	reqresp.MakeRespOk(c)

}

func FinishList(c *gin.Context) {

}

func DownloadList(c *gin.Context) {

	req := &downloadproto.GetDownloadListReq{}
	req.Status = c.Query("status")

	resp, err := downloadbiz.GetDownloadList(c, req)
	if err != nil {
		reqresp.MakeErrMsg(c, err)
		return
	}

	reqresp.MakeResp(c, resp)

}

func RetryDownload(c *gin.Context) {

	req := &downloadproto.RetryDownloadReq{}
	err := reqresp.ParseReq(c, req)
	if err != nil {
		reqresp.MakeErrMsg(c, err)
		return
	}

	err = downloadbiz.RetryDownload(c, req)
	if err != nil {
		reqresp.MakeErrMsg(c, err)
		return
	}

	reqresp.MakeRespOk(c)

}

func Ping(c *gin.Context) {

	startup.Do(func() {
		err := report.Report(&report.ReportReq{
			Action: consts.ActionOpenHomePage,
		})
		if err != nil {
			log.Errorf("startup Report err:%v", err)
		}
	})

	c.String(200, "pong")
}
