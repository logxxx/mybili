package downloadcfg

import (
	"github.com/logxxx/mybili_pkg/utils/log"
	"os"
	"path/filepath"
)

var (
	_cfg *DownloadConfig
)

func init() {
	wd, _ := os.Getwd()
	downloadPath := filepath.Join(wd, "download")
	log.Infof("设置下载目录:%v", downloadPath)
	_cfg = &DownloadConfig{
		DownloadPath: downloadPath,
	}
}

type DownloadConfig struct {
	DownloadPath string `json:"download_path"`
}

func GetConfig() *DownloadConfig {
	return _cfg
}

func SetConfig(cfg *DownloadConfig) {

	if cachePath := os.Getenv("PluginCachePath"); cachePath != "" {
		cfg.DownloadPath = cachePath
	}

	if cfg.DownloadPath == "" {
		wd, _ := os.Getwd()
		cfg.DownloadPath = filepath.Join(wd, "download")
	}
	log.Infof("设置下载目录:%v", cfg.DownloadPath)

	_cfg = cfg
}
