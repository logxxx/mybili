package downloadweb

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/logxxx/mybili_pkg/utils/log"
	"github.com/logxxx/mybili_pkg/utils/reqresp"
	"logxxx.com/mybili/modules/download/downloadcontroller"
	"os"
	"path/filepath"
)

func Init() {
	g := gin.New()
	g.Use(reqresp.Cors())

	g.GET("/ping", downloadcontroller.Ping)
	g.POST("/download", downloadcontroller.Download)
	g.POST("/download/delete", downloadcontroller.DeleteTask)
	g.POST("/download/retry", downloadcontroller.RetryDownload)
	g.GET("/download/list", downloadcontroller.DownloadList)
	g.GET("/video/info", downloadcontroller.GetVideoInfo)
	g.GET("/device/info", downloadcontroller.GetDeviceInfo)
	g.POST("/user/advise", downloadcontroller.UserAdvise)

	pluginWorkDir := os.Getenv("PluginWorkDir")
	//pluginWorkDir := "1"
	log.Printf("demo PluginWorkDir:%v", pluginWorkDir)

	g.GET("/main_page", func(c *gin.Context) {
		filepath.Join(pluginWorkDir, "dist/index.html")
		c.File(filepath.Join(pluginWorkDir, "dist/index.html"))
	})

	g.StaticFS("/dist", gin.Dir(filepath.Join(pluginWorkDir, "dist"), true))

	port := os.Getenv("UIPort")
	log.Printf("UIPort:%v", port)
	if port == "" {
		panic("empty UIPort")
	}

	err := g.Run(fmt.Sprintf(":%v", port))
	if err != nil {
		log.Errorf("downloadweb.Init Run err:%v", err)
	}

}
