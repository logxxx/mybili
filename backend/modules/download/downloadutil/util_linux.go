//go:build linux
//+build linux

package downloadutil

import (
	"syscall"
)

func GetDiskFreeSpace(path string) int64 {
	var stat syscall.Statfs_t

	//unix.RawSyscall()

	syscall.Statfs(path, &stat)

	// Available blocks * size per block = available space in bytes
	size := stat.Bavail * uint64(stat.Bsize)

	return int64(size)
}
