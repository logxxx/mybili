package downloadutil

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	"strings"
)

// source code: https://blog.csdn.net/dotastar00/article/details/108805779
func Bv2av(x string) int64 {

	var (
		table = "fZodR9XQDSUm21yCkr6zBqiveYah8bt4xsWpHnJE7jL5VG3guMTKNPAwcF"
		s     = [6]int{11, 10, 3, 8, 4, 6}
		xor   = 177451812
		add   = 8728348608
	)
	tr := make(map[string]int)
	for i := 0; i < 58; i++ {
		tr[string(table[i])] = i
	}
	r := 0
	for i := 0; i < 6; i++ {
		r += tr[string(x[s[i]])] * int(math.Pow(float64(58), float64(i)))
	}
	return int64((r - add) ^ xor)
}

func RemoveSpecialSymbol(title string) string { // will be used when save the title or the part
	title = strings.Replace(title, ":", "", -1)
	title = strings.Replace(title, "\\", "", -1)
	title = strings.Replace(title, "/", "", -1)
	title = strings.Replace(title, "*", "", -1)
	title = strings.Replace(title, "?", "", -1)
	title = strings.Replace(title, "\"", "", -1)
	title = strings.Replace(title, "<", "", -1)
	title = strings.Replace(title, ">", "", -1)
	title = strings.Replace(title, "|", "", -1)
	title = strings.Replace(title, ".", "", -1)

	return title
}

func HttpGetRaw(url string) (int, []byte, error) {
	httpResp, err := http.Get(url)
	if err != nil {
		return 0, nil, err
	}
	defer httpResp.Body.Close()

	respBytes, err := ioutil.ReadAll(httpResp.Body)
	if err != nil {
		return 0, nil, err
	}

	return httpResp.StatusCode, respBytes, nil
}

func HttpGet(url string, resp interface{}) (int, error) {

	status, respBytes, err := HttpGetRaw(url)
	if err != nil {
		return 0, err
	}

	//log.Debugf("HttpGet\nreq:%v\nresp:%v", url, string(respBytes))

	if resp != nil {
		err = json.Unmarshal(respBytes, resp)
		if err != nil {
			return 0, err
		}
	}

	return status, nil

}

func HttpPost(url string, reqBody interface{}, resp interface{}) (int, error) {

	reqBodyBytes := make([]byte, 0)
	var err error
	if reqBody != nil {
		reqBodyBytes, err = json.Marshal(reqBody)
		if err != nil {
			return 0, err
		}
	}

	reqBodyBuf := bytes.NewBuffer(reqBodyBytes)
	httpResp, err := http.Post(url, "application/json", reqBodyBuf)
	if err != nil {
		return 0, err
	}
	defer httpResp.Body.Close()

	respBytes, err := ioutil.ReadAll(httpResp.Body)
	if err != nil {
		return 0, err
	}

	if resp != nil {
		err = json.Unmarshal(respBytes, resp)
		if err != nil {
			return 0, err
		}
	}

	return httpResp.StatusCode, nil
}

func GetShowSize(input int64) string {
	size := float64(input)
	units := []string{"b", "kb", "MB", "GB", "TB"}
	unitIdx := 0
	for {
		if size < 1024 || unitIdx >= len(units) {
			break
		}
		size /= 1024
		unitIdx++
	}
	return fmt.Sprintf("%.2f%v", size, units[unitIdx])
}
