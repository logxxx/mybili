package downloadconst

const (
	ApiGetCid      = "https://api.bilibili.com/x/web-interface/view"
	ApiGetPlayInfo = "https://interface.bilibili.com/v2/playurl"

	DownloadStatusRunning  = "running"
	DownloadStatusFinished = "finished"
)
