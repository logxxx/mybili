package downloadbiz

import (
	"github.com/logxxx/mybili_pkg/utils/log"
	"github.com/logxxx/mybili_pkg/utils/runutil"
	"github.com/logxxx/mybili_pkg/utils/semaphore"
	"logxxx.com/mybili/modules/download/downloadconst"
	"logxxx.com/mybili/modules/download/downloaddao"
	"logxxx.com/mybili/modules/download/downloadproto"
)

var (
	_mgr *DownloadManager
)

type DownloadManager struct {
	sema          *semaphore.Semaphore
	RunningTasks  []*downloadproto.VideoInfo
	FinishedTasks []*downloadproto.VideoInfo
}

func Init() {
	_mgr = NewDownloadManager()

	_mgr.ContinueLastDownload()

	runutil.RunSafe(_mgr.AutoRefreshToDb)
}

func NewDownloadManager() *DownloadManager {

	mgr := &DownloadManager{
		sema: semaphore.NewSemaphore(1),
	}

	var err error
	mgr.RunningTasks, err = downloaddao.GetDownloadDao().GetTaskList(downloadconst.DownloadStatusRunning)
	if err != nil {
		log.Errorf("NewDownloadManager GetTaskList running err:%v", err)
	}

	mgr.FinishedTasks, err = downloaddao.GetDownloadDao().GetTaskList(downloadconst.DownloadStatusFinished)
	if err != nil {
		log.Errorf("NewDownloadManager GetTaskList finished err:%v", err)
	}

	log.Infof("NewDownloadManager succ")

	return mgr
}

func GetDownloadManager() *DownloadManager {
	return _mgr
}

func (dm *DownloadManager) GetTaskByBvid(status, bvid string) *downloadproto.VideoInfo {

	tasks := dm.getTasks(status)

	for _, t := range tasks {
		if t.Bvid == bvid {
			return t
		}
	}

	return nil
}

func (dm *DownloadManager) AddTask(status string, info *downloadproto.VideoInfo) {
	var taskList []*downloadproto.VideoInfo
	if status == downloadconst.DownloadStatusRunning {
		taskList = dm.RunningTasks
	} else if status == downloadconst.DownloadStatusFinished {
		taskList = dm.FinishedTasks
	}
	taskList = append([]*downloadproto.VideoInfo{info}, taskList...)

	if status == downloadconst.DownloadStatusRunning {
		dm.RunningTasks = taskList
	} else if status == downloadconst.DownloadStatusFinished {
		dm.FinishedTasks = taskList
	}
}

func (dm *DownloadManager) RemoveTask(status, bvid string) {

	taskList := dm.getTasks(status)

	for i := range taskList {
		if taskList[i].Bvid == bvid {
			if i == len(dm.RunningTasks) {
				taskList = taskList[:i]
			} else {
				taskList = append(taskList[:i], taskList[i+1:]...)
			}
			break
		}
	}

	dm.setTasks(status, taskList)
}

func (dm *DownloadManager) getTasks(status string) []*downloadproto.VideoInfo {
	var taskList []*downloadproto.VideoInfo
	if status == downloadconst.DownloadStatusRunning {
		taskList = dm.RunningTasks
	} else if status == downloadconst.DownloadStatusFinished {
		taskList = dm.FinishedTasks
	}
	return taskList
}

func (dm *DownloadManager) setTasks(status string, tasks []*downloadproto.VideoInfo) {
	if status == downloadconst.DownloadStatusRunning {
		dm.RunningTasks = tasks
	} else if status == downloadconst.DownloadStatusFinished {
		dm.FinishedTasks = tasks
	}
}
