package downloadbiz

import (
	"crypto/md5"
	"encoding/base64"
	"errors"
	"fmt"
	"github.com/logxxx/mybili_pkg/utils/fileutil"
	"github.com/logxxx/mybili_pkg/utils/log"
	"github.com/logxxx/mybili_pkg/utils/runutil"
	"logxxx.com/mybili/modules/download/downloadconst"
	"logxxx.com/mybili/modules/download/downloadproto"
	"logxxx.com/mybili/modules/download/downloadutil"
	"logxxx.com/mybili/modules/report"
	"net/http"
	"path/filepath"
	"strings"
)

func GetBidInfoByAid(dpath string, bvid string) (*downloadproto.VideoInfo, error) {

	reqUrl := fmt.Sprintf("%v?bvid=%v", downloadconst.ApiGetCid, bvid)
	resp := &downloadproto.ApiGetBidInfoResp{}
	_, err := downloadutil.HttpGet(reqUrl, resp)
	if err != nil {
		log.Errorf("GetBidInfoByAid HttpGet err:%v", err)
		return nil, err
	}
	log.Infof("resp:%+v", resp)

	picB64 := ""
	if resp.Data.Pic != "" {
		_, picByte, _ := downloadutil.HttpGetRaw(resp.Data.Pic)
		if len(picByte) != 0 {
			picB64 = "data:image/png;base64," + base64.StdEncoding.EncodeToString(picByte)
		}
	}

	videoInfo := &downloadproto.VideoInfo{
		Title: resp.Data.Title,
		Cover: picB64, //浏览器直接访问图片地址会403，所以转化成b64
		Bvid:  resp.Data.Bvid,
		Avid:  resp.Data.Aid,
		//DownloadPath: filepath.Join(dpath, resp.Data.Title),
		DownloadPath: dpath,
		OwnerName:    resp.Data.Owner.Name,
	}

	for _, page := range resp.Data.CidInfos {
		videoInfo.CidInfos = append(videoInfo.CidInfos, &downloadproto.CidInfo{
			Aid:  resp.Data.Aid,
			Cid:  page.Cid,
			Page: page.Page,
			Part: downloadutil.RemoveSpecialSymbol(page.Part),
		})
	}

	return videoInfo, nil

}

func GetPlayVideoUrl(cid int64) (*downloadproto.ApiGetPlayInfoDurl, error) {

	var _entropy = "rbMCKn@KuamXWlPMoJGsKcbiJKUfkPF_8dABscJntvqhRSETg"
	appKey, sec := downloadutil.GetAppKey(_entropy)

	var _paramsTemp = "appkey=%v&cid=%v&otype=json&qn=%v&quality=%v&type="
	var _quality = "80"
	params := fmt.Sprintf(_paramsTemp, appKey, cid, _quality, _quality)
	chksum := fmt.Sprintf("%x", md5.Sum([]byte(params+sec)))

	reqUrl := fmt.Sprintf("%v?%s&sign=%s", downloadconst.ApiGetPlayInfo, params, chksum)
	resp := &downloadproto.ApiGetPlayInfoResp{}
	log.Infof("reqUrl: %v", reqUrl)
	code, err := downloadutil.HttpGet(reqUrl, resp)
	if err != nil {
		log.Errorf("GetPlayVideoUrl HttpGet err:%v", err)
		return nil, err
	}
	if code != http.StatusOK {
		return nil, fmt.Errorf("invalid code:%v", code)
	}

	if len(resp.Durl) <= 0 {
		return nil, fmt.Errorf("empty durl")
	}

	return &resp.Durl[0], nil
}

func ParseLinksToBvids(rawLink string) ([]string, error) {

	links := strings.Split(rawLink, "\n")
	if len(links) <= 0 {
		return nil, errors.New("parse link failed")
	}

	log.Infof("ParseLinksToBvids links:%v", links)

	bvids := make([]string, 0)

	for _, link := range links {
		bvid, err := ParseLinkToBvid(link)
		if err != nil {
			log.Errorf("ParseLinksToBvids ParseLinkToBvid err:%v link:%v", err, link)
			return nil, err
		}
		if bvid != "" {
			bvids = append(bvids, bvid)
		}
	}

	return bvids, nil

}

func ParseLinkToBvid(link string) (string, error) {

	//去掉空格之类的东西
	link = strings.TrimSpace(link)

	if IsBvid(link) {
		return link, nil
	}

	prefix := "www.bilibili.com/video/"
	startIdx := strings.Index(link, "www.bilibili.com/video/")
	if startIdx <= 0 {
		return "", errors.New("invalid link")
	}

	//TODO:用正则
	endIdx := strings.Index(link, "/?") //有的会在bvid后加/?而不是?，这里做下兼容。
	if endIdx <= 0 {
		endIdx = strings.Index(link, "?")
		if endIdx <= 0 {
			return "", errors.New("invalid link")
		}
	}

	bvid := link[startIdx+len(prefix) : endIdx]

	if !IsBvid(bvid) {
		return "", errors.New("invalid link")
	}

	return bvid, nil
}

func IsBvid(input string) bool {
	//BV1R94y197Va
	if len(input) != 12 {
		return false
	}

	if input[:2] != "BV" {
		return false
	}

	return true

}

func getUniqPath(base, title string) (string, string) {

	i := 1
	for {
		newTitle := fmt.Sprintf("%v(%v)", title, i)
		newPath := filepath.Join(base, newTitle)
		if !fileutil.IsExists(newPath) {
			return newTitle, newPath
		}
		i++
	}
}

func reportDownload(action string, req interface{}, err error) {

	payload := struct {
		Req interface{} `json:"request"`
		Err error       `json:"error"`
	}{
		Req: req,
		Err: err,
	}

	reportReq := &report.ReportReq{
		Action:  action,
		Payload: payload,
	}

	runutil.RunSafe(func() {
		reportErr := report.Report(reportReq)
		if reportErr != nil {
			log.Errorf("reportDownload Report err:%v req:%v", err, log.JSON(reportReq))
		}
	})
}
