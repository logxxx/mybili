package downloadbiz

import (
	"errors"
	"github.com/logxxx/mybili_pkg/utils/log"
	"logxxx.com/mybili/modules/download/downloadconst"
	"logxxx.com/mybili/modules/download/downloaddao"
	"logxxx.com/mybili/modules/download/downloadproto"
	"logxxx.com/mybili/modules/downloader"
	"time"
)

func (dm *DownloadManager) ContinueLastDownload() {

	log.Infof("开始导入未完成的下载任务")

	succCount := 0
	for _, task := range GetDownloadManager().RunningTasks {
		for _, cidInfo := range task.CidInfos {
			var err error
			err = downloadCore(task.DownloadPath, cidInfo, false)
			if err != nil {
				log.Errorf("DownloadManager.ContinueLastDownload err:%v cidInfo:%+v", err, log.JSON(cidInfo))
			} else {
				succCount++
				log.Infof("成功导入任务:%v_%v", task.Title, cidInfo.Part)
			}
		}
	}
	log.Infof("导入结束!共导入%v个任务", succCount)

}

func (dm *DownloadManager) refreshTasks() error {

	ok := dm.sema.TryAcquire()
	if !ok {
		return nil
	}
	defer dm.sema.Release()

	//1.更新每个任务信息
	for _, videoInfo := range dm.RunningTasks {
		for _, cidInfo := range videoInfo.CidInfos {

			err := updateCidInfo(cidInfo)
			if err != nil {
				log.Errorf("refreshTasks updateCidInfo err:%v", err)
				return err
			}
		}
	}

	//2.把已下载完成的移动到已完成列表里
	for _, task := range dm.RunningTasks {
		doneNum := 0
		for _, cidInfo := range task.CidInfos {
			if cidInfo.RunTime.FinishSize == cidInfo.RunTime.TotalSize {
				doneNum++
			}
		}

		if doneNum != len(task.CidInfos) {
			continue
		}

		log.Infof("refreshTasks 移动任务到【已完成】列表:%v", task.Title)

		task.FinishTime = time.Now().Unix()
		dm.RemoveTask(downloadconst.DownloadStatusRunning, task.Bvid)
		dm.AddTask(downloadconst.DownloadStatusFinished, task)

	}

	return nil
}

func updateCidInfo(cidInfo *downloadproto.CidInfo) error {
	status, err := downloader.GetDownloadStatus(cidInfo.RunTime.TaskID)
	if err != nil {
		log.Errorf("refreshTasks GetDownloadStatus err:%v taskID:%v", err, cidInfo.RunTime.TaskID)
		return err
	}
	cidInfo.RunTime.StopTime = status.StopTime
	cidInfo.RunTime.FinishSize = status.FinishSize
	cidInfo.RunTime.TotalSize = status.TotalSize
	cidInfo.RunTime.Speed = status.Speed
	cidInfo.RunTime.ErrMsg = status.ErrMsg
	cidInfo.RunTime.UpdateTime = time.Now().Unix()
	return nil
}

func (dm *DownloadManager) AutoRefreshToDb() {
	for {
		select {
		case <-time.After(3 * time.Second):
			err := dm.refreshTasks()
			if err != nil {
				log.Errorf("AutoRefreshToDb refreshTasks err:%v", err)
			}
			//log.Infof("设置%v个【下载中】任务到db", len(dm.RunningTasks))
			err = downloaddao.GetDownloadDao().SetTaskList(downloadconst.DownloadStatusRunning, dm.RunningTasks)
			if err != nil {
				log.Errorf("AutoRefreshToDb SetTaskList err:%v", err)
			}
			//log.Infof("设置%v个【已完成】任务到db", len(dm.FinishedTasks))
			err = downloaddao.GetDownloadDao().SetTaskList(downloadconst.DownloadStatusFinished, dm.FinishedTasks)
			if err != nil {
				log.Errorf("AutoRefreshToDb SetTaskList err:%v", err)
			}
		}
	}
}

func (dm *DownloadManager) refreshAndGetTasks(status string) ([]*downloadproto.VideoInfo, error) {
	err := dm.refreshTasks()
	if err != nil {
		log.Errorf("GetDownloadInfos refreshTasks err:%v", err)
	}

	var destTasks []*downloadproto.VideoInfo
	if status == downloadconst.DownloadStatusRunning {
		destTasks = dm.RunningTasks
	} else if status == downloadconst.DownloadStatusFinished {
		destTasks = dm.FinishedTasks
	} else {
		return nil, errors.New("invlid task status")
	}

	return destTasks, nil
}

func (dm *DownloadManager) GetDownloadInfos(status string) (*downloadproto.GetDownloadListResp, error) {

	destTasks, err := dm.refreshAndGetTasks(status)
	if err != nil {
		return nil, err
	}

	items := make([]*downloadproto.GetDownloadListItem, 0)
	for _, downloadInfo := range destTasks {
		item := &downloadproto.GetDownloadListItem{
			Bvid:         downloadInfo.Bvid,
			Avid:         downloadInfo.Avid,
			Title:        downloadInfo.Title,
			Cover:        downloadInfo.Cover,
			DownloadPath: downloadInfo.DownloadPath,
			FinishTime:   downloadInfo.FinishTime,
		}
		items = append(items, item)
		for _, cidInfo := range downloadInfo.CidInfos {
			if cidInfo.RunTime.TaskID == "" {
				continue
			}
			subItem := &downloadproto.GetDownloadListSubItem{
				Cid:        cidInfo.Cid,
				FileDir:    cidInfo.RunTime.FileDir,
				FileName:   cidInfo.RunTime.FileName,
				TotalSize:  cidInfo.RunTime.TotalSize,
				FinishSize: cidInfo.RunTime.FinishSize,
				Speed:      cidInfo.RunTime.Speed,
				ErrMsg:     cidInfo.RunTime.ErrMsg,
			}
			if cidInfo.RunTime.Speed > 0 {
				subItem.TimeLeftSec = cidInfo.RunTime.TotalSize / cidInfo.RunTime.Speed
			}
			item.SubItems = append(item.SubItems, subItem)
			item.Speed += subItem.Speed
			item.TotalSize += subItem.TotalSize
			item.FinishSize += subItem.FinishSize
			if subItem.ErrMsg != "" { //最后一个错误
				item.ErrMsg = subItem.ErrMsg
			}
			if item.Speed > 0 {
				item.TimeLeftSec = item.TotalSize / item.Speed
			}
		}
	}

	resp := &downloadproto.GetDownloadListResp{
		Items: items,
	}

	if len(resp.Items) > 0 {
		resp.ExpireSec = 1
	} else {
		resp.ExpireSec = 10
	}

	return resp, nil
}
