package downloadbiz_test

import (
	"context"
	"github.com/logxxx/mybili_pkg/utils/fileutil"
	"github.com/logxxx/mybili_pkg/utils/log"
	"github.com/stretchr/testify/assert"
	"io"
	"logxxx.com/mybili/modules/download/downloadbiz"
	"logxxx.com/mybili/modules/download/downloadproto"
	"logxxx.com/mybili/modules/downloader"
	"logxxx.com/mybili/modules/downloader/danmu"
	"os"
	"testing"
	"time"
)

var (
	ctx = context.Background()
)

func TestGetVideoInfo(t *testing.T) {

	req1 := &downloadproto.GetVideoInfoReq{
		Bvid: "BV1wr4y1g7Df",
	}
	resp1, err := downloadbiz.GetVideoInfo(ctx, req1)
	assert.Nil(t, err)
	log.JSONGrace(resp1)
}

func TestDownloadByBvid(t *testing.T) {

	downloadbiz.SetCookie(`buvid3=9734FB43-3749-42DE-A60D-969A87DBE3CD31013infoc; b_nut=1651219229; i-wanna-go-back=-1; CURRENT_BLACKGAP=0; buvid_fp_plain=undefined; PVID=1; header_theme_version=CLOSE; buvid4=CDD7D4CF-E8CF-BF8D-81DE-A9629BE9722F31013-022042916-aqrJQQc0ovEXjjPAs8PP8Qg%2FKd%2BOyDIn025ojCmRffAyw2L5%2BC6jAw%3D%3D; CURRENT_FNVAL=4048; CURRENT_PID=bacf4740-ce08-11ed-9dc1-1f04169e7bb7; _uuid=CC710F25E-C6ED-DF85-10AEB-B9FBB7FD9EB1089795infoc; nostalgia_conf=-1; LIVE_BUVID=AUTO8316843864962536; b_ut=7; FEED_LIVE_VERSION=V8; iflogin_when_web_push=1; rpdid=|(J|~|Ru)~uR0J'u~|lJ~)Jm~; bp_video_offset_261168106=890467331737649170; enable_web_push=DISABLE; home_feed_column=4; browser_resolution=1138-953; bili_ticket=eyJhbGciOiJIUzI1NiIsImtpZCI6InMwMyIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MDg2NjQ0MTIsImlhdCI6MTcwODQwNTE1MiwicGx0IjotMX0.UrXok8rEOK-5O9hC9P9pdCiE9arwUYZT_WXDgfCOPM4; bili_ticket_expires=1708664352; bsource=search_baidu; b_lsid=64C14C8C_18DC62C630C; fingerprint=7fdc5a884be2c69dbf50d120a68b1182; SESSDATA=b57709ad%2C1723981333%2C3a0db%2A22CjBKBq2VfyzVG6IDjUtKSkb0gfnoihNk9yvW1YEtoXpqglK5_4Ui94aQdH7yAw6_x1MSVmNsU0J0T3l6b0ZqZ3NydU5tYmo1dE1TQnEtOUNURlpzWFdNeW1fUHBmblZ3Nm5qYTFMUXh0R2VXRVJiZTRUZG01R1JvTkhBU1hZV3dJY3I1dXk1eV9RIIEC; bili_jct=0999a59af036bcce73255d6365ea94c3; DedeUserID=261168106; DedeUserID__ckMd5=8c0fd3d8858dda71; buvid_fp=a86f975e4f6d3a72b6df1045dd797572; sid=fc5tcrbv`)

	_, err := downloadbiz.DownloadByBvid("BV1by421b78F", `D:\hhy\mytest\mywork\mybili\backend\modules\download\downloadbiz`)
	if err != nil {
		t.Fatal(err)
	}
}

func TestDownloadDanmu(t *testing.T) {

	//https://api.bilibili.com/x/v2/dm/web/seg.so?type=1&oid=750976589&pid=770217424&segment_index=1

	wd, _ := os.Getwd()
	t.Logf("wd:%v", wd)

	aid, cid, err := downloadbiz.GetVideoInfoByBvidSimple("BV1TL4y1n7if")
	assert.Nil(t, err)

	req1 := &downloadproto.DownloadDanmuReq{
		Aid:      aid,
		Cid:      cid,
		FileDir:  wd,
		FileName: time.Now().Format("test150405"),
	}
	err = danmu.DownloadDanmu(req1)
	assert.Nil(t, err)
}

func TestParseLinkToBvid(t *testing.T) {
	req1 := "https://www.bilibili.com/video/BV1R94y197Va?spm_id_from=333.851.b_7265636f6d6d656e64.1&vd_source=758cf70b3ec4f0cd2e584ce32bb07555"
	resp1, err := downloadbiz.ParseLinkToBvid(req1)
	assert.Nil(t, err)
	assert.Equal(t, "BV1R94y197Va", resp1)

	req2 := "BV1R94y197Va"
	resp2 := downloadbiz.IsBvid(req2)
	assert.True(t, resp2)

	req3 := "BV1R94y197Vaa"
	resp3 := downloadbiz.IsBvid(req3)
	assert.False(t, resp3)

	req4 := "AV1R94y197Vaa"
	resp4 := downloadbiz.IsBvid(req4)
	assert.False(t, resp4)

	req5 := `https://www.bilibili.com/video/BV1uB4y1W7k3?spm_id_from=333.851.b_7265636f6d6d656e64.2
https://www.bilibili.com/video/BV1LU4y1Q7wF?spm_id_from=333.851.b_7265636f6d6d656e64.3
https://www.bilibili.com/video/BV1vZ4y1i7Rr?spm_id_from=333.851.b_7265636f6d6d656e64.5&vd_source=758cf70b3ec4f0cd2e584ce32bb07555
https://www.bilibili.com/video/BV1YT41137eb/?spm_id_from=333.788.recommend_more_video.0&vd_source=758cf70b3ec4f0cd2e584ce32bb07555
https://www.bilibili.com/video/BV143411u7jc/?spm_id_from=333.788.recommend_more_video.1&vd_source=758cf70b3ec4f0cd2e584ce32bb07555`
	resp5, err := downloadbiz.ParseLinksToBvids(req5)
	assert.Nil(t, err)
	t.Logf("resp5:%v", resp5)

}

func TestGetMp4Url(t *testing.T) {
	aid := int64(770217424)
	cid := int64(750976589)
	resp1, _, err := downloadbiz.GetMp4Url(aid, cid)
	assert.Nil(t, err)
	t.Logf("resp1:%v", resp1)

	buf, err := downloader.GetDownloadBuf(aid, 0, resp1, 0)
	assert.Nil(t, err)

	wd, _ := os.Getwd()
	t.Logf("wd:%v", wd)

	file, _, err := fileutil.GetOrCreateFile(".", "test")
	assert.Nil(t, err)
	defer file.Close()

	_, err = io.Copy(file, buf)
	assert.Nil(t, err)

}
