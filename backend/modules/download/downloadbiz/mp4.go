package downloadbiz

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/logxxx/mybili_pkg/utils/log"
	"io/ioutil"
	"net/http"
	"strings"
)

var (
	_cookie = ""
)

func SetCookie(c string) {
	_cookie = c
}

type PlayUrlResp struct {
	Code    int             `json:"code"`
	Message string          `json:"message"`
	Data    PlayUrlRespData `json:"data"`
}

type PlayUrlRespData struct {
	Durl           []PlayUrlRespDurl          `json:"durl"`
	SupportFormats []PlayUrlRespSupportFormat `json:"support_formats"`
}

type PlayUrlRespDurl struct {
	Url       string `json:"url"`
	Size      int64  `json:"size"`
	BackupUrl string `json:"backup_url"`
}

type PlayUrlRespSupportFormat struct {
	Quality        int    `json:"quality"`
	Format         string `json:"format"`
	NewDescription string `json:"new_description"`
}

func GetMp4Url(aid int64, cid int64) (string, int64, error) {
	url := fmt.Sprintf("https://api.bilibili.com/x/player/playurl?avid=%v&cid=%v&qn=116&type=&otype=json&platform=html5&high_quality=1", aid, cid)

	httpReq, _ := http.NewRequest("GET", url, nil)
	httpReq.Header.Set("Cookie", _cookie)

	httpResp, err := http.DefaultClient.Do(httpReq)
	if err != nil {
		log.Errorf("GetMp4Url err:%v", err)
		return "", 0, err
	}

	defer func() {
		httpResp.Body.Close()
	}()

	if httpResp.StatusCode != http.StatusOK {
		err = errors.New("invlid code")
		log.Errorf("GetMp4Url HttpGet err:%v code:%v", err, httpResp.StatusCode)
		return "", 0, err
	}

	respBytes, err := ioutil.ReadAll(httpResp.Body)
	if err != nil {
		log.Errorf("GetMp4Url err:%v", err)
		return "", 0, err
	}

	resp := &PlayUrlResp{}
	err = json.Unmarshal(respBytes, resp)
	if err != nil {
		log.Errorf("GetMp4Url err:%v", err)
		return "", 0, err
	}

	log.Infof("GetMp4Url resp:%+v", log.JSONGrace(resp))

	if len(resp.Data.Durl) <= 0 {
		return "", 0, errors.New("empty durl")
	}

	return TransHtmlJson(resp.Data.Durl[0].Url), resp.Data.Durl[0].Size, nil
}

func TransHtmlJson(data string) string {
	data = strings.Replace(data, "\\u0026", "&", -1)
	data = strings.Replace(data, "\\u003c", "<", -1)
	data = strings.Replace(data, "\\u003e", ">", -1)
	return data
}
