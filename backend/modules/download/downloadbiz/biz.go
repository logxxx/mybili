package downloadbiz

import (
	"context"
	"errors"
	"fmt"
	"github.com/logxxx/mybili_pkg/consts"
	"github.com/logxxx/mybili_pkg/utils/fileutil"
	"github.com/logxxx/mybili_pkg/utils/log"
	"github.com/logxxx/mybili_pkg/utils/runutil"
	"logxxx.com/mybili/modules/basecfg"
	"logxxx.com/mybili/modules/download/downloadcfg"
	"logxxx.com/mybili/modules/download/downloadconst"
	"logxxx.com/mybili/modules/download/downloadproto"
	"logxxx.com/mybili/modules/download/downloadutil"
	"logxxx.com/mybili/modules/downloader"
	"logxxx.com/mybili/modules/downloader/danmu"
	"logxxx.com/mybili/modules/version"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"
)

func GetDownloadList(ctx context.Context, req *downloadproto.GetDownloadListReq) (*downloadproto.GetDownloadListResp, error) {

	if req.Status == "" {
		req.Status = downloadconst.DownloadStatusRunning
	}

	return GetDownloadManager().GetDownloadInfos(req.Status)
}

func GetVideoInfo(ctx context.Context, req *downloadproto.GetVideoInfoReq) (*downloadproto.GetVideoInfoResp, error) {
	videoInfo, err := GetVideoInfoByBvid(downloadcfg.GetConfig().DownloadPath, req.Bvid)
	if err != nil {
		return nil, err
	}

	resp := &downloadproto.GetVideoInfoResp{
		VideoInfo: videoInfo,
	}

	return resp, nil
}

// return: aid + cid
func GetVideoInfoByBvidSimple(bvid string) (aid, cid int64, err error) {
	videoInfo, err := GetVideoInfoByBvid(downloadcfg.GetConfig().DownloadPath, bvid)
	if err != nil {
		return
	}

	if len(videoInfo.CidInfos) <= 0 {
		return
	}

	return videoInfo.Avid, videoInfo.CidInfos[0].Cid, nil

}

func GetVideoInfoByBvid(dpath, bvid string) (*downloadproto.VideoInfo, error) {
	if bvid == "" {
		return nil, errors.New("empty bvid")
	}

	//avid := downloadutil.Bv2av(bvid)
	//if avid <= 0 {
	//	return nil, errors.New("invalid bvid")
	//}

	videoInfo, err := GetBidInfoByAid(dpath, bvid)
	if err != nil {
		log.Errorf("CreateDownload GetBidInfoByAid err:%v", err)
		return nil, err
	}

	return videoInfo, nil
}

func DeleteDownload(ctx context.Context, req *downloadproto.DeleteDownloadReq) (err error) {

	defer func() {
		reportDownload(consts.ActionDeleteTask, req, err)
	}()

	if req.Bvid == "" {
		err = errors.New("empty bvid")
		return
	}

	task := GetDownloadManager().GetTaskByBvid(req.Status, req.Bvid)

	if task == nil {
		return errors.New("task not found")
	}

	//2.停止任务
	err = stopTask(task)
	if err != nil {
		log.Errorf("DeleteDownload stopTask err:%v", err)
		return err
	}

	GetDownloadManager().RemoveTask(req.Status, req.Bvid)

	if req.WithFile {
		err := os.RemoveAll(task.DownloadPath)
		if err != nil {
			log.Errorf("DeleteDownload Remove err:%v path:%v", err, task.DownloadPath)
			return err
		}
	}

	return nil
}

func CreateDownload(ctx context.Context, req *downloadproto.CreateDownloadReq) (err error) {

	defer reportDownload(consts.ActionCreateTask, req, err)

	if req.Links == "" {
		err = errors.New("empty link")
		log.Errorf("CreateDownload err:%v", err)
		return err
	}

	bvids, err := ParseLinksToBvids(req.Links)
	if err != nil {
		log.Errorf("CreateDownload ParseLinksToBvids err:%v req:%v", err, log.JSON(req))
		return err
	}

	if len(bvids) <= 0 {
		err = errors.New("bvid not found")
		log.Errorf("CreateDownload ParseLinksToBvids err:%v req:%v", err, log.JSON(req))
		return err
	}

	for _, bvid := range bvids {

		bvidReq := &downloadproto.CreateDownloadReq{
			Bvid:      bvid,
			Force:     req.Force,
			NeedDanmu: req.NeedDanmu,
		}

		videoInfo, err := downloadByBvid(bvidReq)
		if err != nil {
			log.Errorf("CreateDownload downloadByBvid err:%v", err)
			return err
		}

		GetDownloadManager().AddTask(downloadconst.DownloadStatusRunning, videoInfo)
	}

	return nil

}

func GetDeviceInfo(ctx context.Context) (*downloadproto.GetDeviceInfoResp, error) {
	freeSize := downloadutil.GetDiskFreeSpace(downloadcfg.GetConfig().DownloadPath)
	showFreeSize := downloadutil.GetShowSize(freeSize)
	log.Infof("GetDeviceInfo DownloadPath:%v freeSize:%v show:%v", downloadcfg.GetConfig().DownloadPath, freeSize, showFreeSize)

	resp := &downloadproto.GetDeviceInfoResp{
		DownloadPath:  downloadcfg.GetConfig().DownloadPath,
		DiskFreeSpace: showFreeSize,
		ClientVersion: version.GetVersion(),
		StartupTime:   version.GetStartupTime(),
		PosterUrl:     basecfg.GetConfig().PosterUrl,
	}

	return resp, nil
}

func RetryDownload(ctx context.Context, req *downloadproto.RetryDownloadReq) (err error) {

	defer func() {
		reportDownload(consts.ActionRetryTask, req, err)
	}()

	if req.Bvid == "" && req.Cid == 0 {
		return errors.New("empty bvid and cid")
	}

	if req.Cid == 0 {
		//重试整个任务
		return retryTask(ctx, req.Bvid)
	} else {
		//重试子任务
		return retrySubTask(ctx, req.Bvid, req.Cid)
	}

}

func retryTask(ctx context.Context, bvid string) error {

	//1.获取任务
	task := GetDownloadManager().GetTaskByBvid(downloadconst.DownloadStatusRunning, bvid)
	if task == nil {
		err := errors.New("task not found")
		log.Errorf("retryTask getRunningTaskByBvid err:%v", err)
		return err
	}

	//2.停止任务
	err := stopTask(task)
	if err != nil {
		log.Errorf("retryTask stopTask err:%v", err)
		return err
	}

	//3.从任务列表中移除
	GetDownloadManager().RemoveTask(downloadconst.DownloadStatusRunning, task.Bvid)

	//4.重新下载
	downloadReq := &downloadproto.CreateDownloadReq{
		Bvid:  bvid,
		Force: true,
	}
	newTask, err := downloadByBvid(downloadReq)
	if err != nil {
		log.Errorf("retryTask downloadByBvid err:%v", err)
		return err
	}

	//5.加入到下载队列
	GetDownloadManager().AddTask(downloadconst.DownloadStatusRunning, newTask)

	return nil

}

func retrySubTask(ctx context.Context, bvid string, cid int64) error {

	//1.获取任务
	task := GetDownloadManager().GetTaskByBvid(downloadconst.DownloadStatusRunning, bvid)
	if task == nil {
		return errors.New("task not found")
	}

	//2.获取子任务
	var cidInfo *downloadproto.CidInfo
	for _, c := range task.CidInfos {
		if cid == c.Cid {
			cidInfo = c
			break
		}
	}

	if cidInfo == nil {
		return fmt.Errorf("cidIndo not found.bvid:%v cid:%v", bvid, cid)
	}

	//3.停止子任务
	if cidInfo.RunTime.TaskID != "" {
		err := downloader.StopDownload(cidInfo.RunTime.TaskID)
		if err != nil {
			log.Errorf("retrySubTask StopDownload err:%v taskID:%v", err, cidInfo.RunTime.TaskID)
		}
	}

	//4.重新下载子任务
	newCidInfo, err := doDownloadSpecificCid(bvid, cid)
	if err != nil {
		return err
	}

	//5.替换子任务信息
	task = GetDownloadManager().GetTaskByBvid(downloadconst.DownloadStatusRunning, bvid)
	if task == nil {
		return errors.New("task not found")
	}

	for i := range task.CidInfos {
		if cid == task.CidInfos[i].Cid {
			task.CidInfos[i] = newCidInfo
		}
	}

	return nil

}

func stopTask(task *downloadproto.VideoInfo) error {

	for _, cidInfo := range task.CidInfos {
		if cidInfo.RunTime.TaskID == "" {
			continue
		}
		err := downloader.StopDownload(cidInfo.RunTime.TaskID)
		if err != nil {
			log.Errorf("retryTask StopDownload err:%v taskID:%v", err, cidInfo.RunTime.TaskID)
		}
	}

	return nil
}

func DownloadByBvidByBBDown(bvid string) (interface{}, error) {
	st := time.Now()

	exists := false
	wd, _ := os.Getwd()
	dirs, _ := os.ReadDir(wd)
	for _, dir := range dirs {
		if strings.Contains(dir.Name(), bvid) {
			exists = true
			break
		}
	}
	if exists {
		return nil, nil
	}

	output, err := exec.Command(`D:\hhy\mytest\mywork\BBDown_1.6.2_20240512_win-x64\BBDown.exe`, bvid, "-F", "<ownerName>_<bvid>_<videoTitle>_<pageNumber>").CombinedOutput()
	if err != nil {
		log.Errorf("DownloadByBvidByBBDown err:%v", err)
		return nil, err
	}
	log.Infof("DownloadByBvidByBBDown succ! cost:%.2fs", time.Since(st).Seconds())
	//log.Infof("DownloadByBvidByBBDown output:%v", string(output))
	_ = output

	return nil, nil
}

func DownloadByBvid(bvid string, dpath string) (interface{}, error) {
	videoInfo, err := GetVideoInfoByBvid(dpath, bvid)
	if err != nil {
		return nil, err
	}

	for _, cidInfo := range videoInfo.CidInfos {
		cidInfo.OwnerName = videoInfo.OwnerName
		err = downloadCore(videoInfo.DownloadPath, cidInfo, true)
		if err != nil {
			return nil, err
		}
	}
	return nil, nil
}

func doDownloadSpecificCid(bvid string, cid int64) (*downloadproto.CidInfo, error) {

	videoInfo, err := GetVideoInfoByBvid(downloadcfg.GetConfig().DownloadPath, bvid)
	if err != nil {
		return nil, err
	}

	var cidInfo *downloadproto.CidInfo
	for _, ci := range videoInfo.CidInfos {
		if ci.Cid == cid {
			cidInfo = ci
			break
		}
	}

	if cidInfo == nil {
		return nil, fmt.Errorf("cid info not found.bvid:%v cid:%v", bvid, cid)
	}

	err = downloadCore(videoInfo.DownloadPath, cidInfo, false)
	if err != nil {
		return nil, err
	}

	return cidInfo, nil

}

// 异步下载弹幕
func downloadDanmuAsync(downloadPath string, cidInfo *downloadproto.CidInfo) error {
	runutil.RunSafe(func() {
		err := downloadDanmu(downloadPath, cidInfo)
		if err != nil {
			log.Errorf("downloadDanmuAsync err:%v", err)
		}
	})
	return nil
}

func downloadDanmu(downloadPath string, cidInfo *downloadproto.CidInfo) error {
	//下载弹幕
	danmuReq := &downloadproto.DownloadDanmuReq{
		Cid:      cidInfo.Cid,
		Aid:      cidInfo.Aid,
		FileDir:  downloadPath,
		FileName: fmt.Sprintf("%v_%v", cidInfo.Page, cidInfo.Part),
	}
	err := danmu.DownloadDanmu(danmuReq)
	if err != nil {
		log.Errorf("downloadCore DownloadDanmu err:%v req:%+v", err, log.JSONGrace(danmuReq))
		return err
	}

	return nil

}

func downloadCore(downloadPath string, cidInfo *downloadproto.CidInfo, directly bool) error {

	prefix := "mp4"
	url, size, err := GetMp4Url(cidInfo.Aid, cidInfo.Cid)
	if err != nil {
		log.Errorf("downloadCore GetMp4Url err:%v", err)

		//兜底
		playDurl, err := GetPlayVideoUrl(cidInfo.Cid)
		if err != nil {
			log.Errorf("downloadCore GetPlayVideoUrl err:%v", err)
			return err
		}

		url = playDurl.Url
		size = playDurl.Size
		prefix = "flv"
	}

	downloadReq := &downloadproto.DownloadVideoReq{
		Aid:      cidInfo.Aid,
		Cid:      cidInfo.Cid,
		Page:     cidInfo.Page,
		VideoUrl: url,
		Size:     size,
		FileDir:  filepath.Join(downloadPath, time.Now().Format("0102")),
		FileName: fmt.Sprintf("%v_%v.%v", cidInfo.OwnerName, cidInfo.Part, prefix),
	}
	if cidInfo.Page > 1 {
		downloadReq.FileName = fmt.Sprintf("%v_%v_%v.%v", cidInfo.OwnerName, cidInfo.Page, cidInfo.Part, prefix)
	}

	log.Printf("downloadCore get mp4 url:%v\ndir:%v filename:%v", url, downloadReq.FileDir, downloadReq.FileName)

	if directly {
		err := downloader.DownloadVideoDirectly(downloadReq)
		if err != nil {
			log.Errorf("downloadCore DownloadVideo err:%v", err)
			return err
		}
	} else {
		resp, err := downloader.DownloadVideo(downloadReq)
		if err != nil {
			log.Errorf("downloadCore DownloadVideo err:%v", err)
			return err
		}

		cidInfo.RunTime = downloadproto.CidRuntime{
			CreateTime: time.Now().Unix(),
			UpdateTime: time.Now().Unix(),
			TaskID:     resp.TaskID,
			FileDir:    downloadReq.FileDir,
			FileName:   downloadReq.FileName,
		}
	}

	return nil

}

// sureDuplicate: 用户确认要重复下载。为true时，会在文件夹名称后追加(1)/(2)/(3)...直到不重复。
func downloadByBvid(req *downloadproto.CreateDownloadReq) (*downloadproto.VideoInfo, error) {

	videoInfo, err := GetVideoInfoByBvid(downloadcfg.GetConfig().DownloadPath, req.Bvid)
	if err != nil {
		return nil, err
	}

	isExist := fileutil.IsExists(videoInfo.DownloadPath)
	if isExist {
		log.Infof("downloadByBvid 发现目标路径已存在:%v", videoInfo.DownloadPath)
		if !req.Force {
			return nil, errors.New("duplicate download path")
		}
		videoInfo.Title, videoInfo.DownloadPath = getUniqPath(downloadcfg.GetConfig().DownloadPath, videoInfo.Title)
		log.Infof("找到了不重复的下载目录:%v", videoInfo.DownloadPath)
	}

	for _, cidInfo := range videoInfo.CidInfos {

		if req.NeedDanmu {
			err = downloadDanmuAsync(videoInfo.DownloadPath, cidInfo)
			if err != nil {
				log.Errorf("downloadByBvid downloadDanmuAsync err:%v req:%v", err, log.JSONGrace(req))
				return nil, err
			}
		}

		err := downloadCore(videoInfo.DownloadPath, cidInfo, false)
		if err != nil {
			log.Errorf("downloadByBvid downloadCore err:%v", err)
			return nil, err
		}
	}

	return videoInfo, nil
}
