package report

import (
	"encoding/json"
	"github.com/logxxx/mybili_pkg/proto"
	"github.com/logxxx/mybili_pkg/utils/log"
	"github.com/logxxx/mybili_pkg/utils/netutil"
	"github.com/logxxx/mybili_pkg/utils/runutil"
	"logxxx.com/mybili/modules/report/reportcfg"
	"logxxx.com/mybili/modules/version"
	"os"
	"runtime"
)

type ReportReq struct {
	Action  string
	Payload interface{}
}

func Report(req *ReportReq) error {

	url := reportcfg.GetConfig().ReportApi

	if req.Payload == nil {
		req.Payload = struct{}{}
	}

	payloadJson, _ := json.Marshal(req.Payload)

	httpReq := &proto.ReportRequest{
		UserID:   os.Getenv("THUNDER_USERID"),
		UserType: 1,
		Action:   req.Action,
		Payload:  string(payloadJson),
		Runtime: proto.Runtime{
			IP:              runutil.GetIP(),
			MacAddr:         runutil.GetMacAddr(),
			DeviceID:        "",
			Platform:        os.Getenv("PLATFORM"),
			PlatformVersion: "",
			ClientVersion:   version.GetVersion(),
			OS:              runtime.GOOS,
			Arch:            runtime.GOARCH,
		},
	}

	_, err := netutil.HttpPost(url, httpReq, nil)
	if err != nil {
		log.Errorf("Report HttpPost err:%v url:%v httpReq:%v", err, url, log.JSON(httpReq))
		return err
	}

	return nil

}
