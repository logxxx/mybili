package reportcfg

var (
	_cfg *ReportConfig
)

type ReportConfig struct {
	ReportApi string `json:"report_api" yaml:"report_api"`
}

func GetConfig() *ReportConfig {
	return _cfg
}

func SetConfig(cfg *ReportConfig) {
	_cfg = cfg
}
