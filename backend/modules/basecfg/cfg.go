package basecfg

var (
	_cfg *BaseConfig
)

type BaseConfig struct {
	PosterUrl string `json:"poster_url" yaml:"poster_url"`
}

func GetConfig() *BaseConfig {
	return _cfg
}

func SetConfig(cfg *BaseConfig) {
	_cfg = cfg
}
