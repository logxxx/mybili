package version

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetMaxVersion(t *testing.T) {
	req1 := []string{
		"v1.0.0",
		"v1.0.1",
		"v9.0.0",
		"v1.0.2",
	}
	resp1 := GetMaxVersion(req1)
	assert.Equal(t, "v9.0.0", resp1)
}

func TestGetVersion(t *testing.T) {

	version := GetVersion()
	t.Logf("version:%v", version)
}
