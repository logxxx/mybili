package version

import (
	"embed"
	"github.com/logxxx/mybili_pkg/utils/log"
	"golang.org/x/mod/semver"
	"io/fs"
	"os"
	"strings"
	"time"
)

var (
	_version     = ""
	_startupTime time.Time
)

//go:embed changelog
var changelogDirFs embed.FS

func init() {
	_version = calVersion()
	_startupTime = time.Now()
}

func GetVersion() string {
	return _version
}

func GetStartupTime() int64 {
	return _startupTime.Unix()
}

func calVersion() (version string) {

	version = "unknown"

	files, err := fs.ReadDir(changelogDirFs, "changelog")
	if err != nil {
		wd, _ := os.Getwd()
		log.Errorf("GetVersion err:%v wd:%v", err, wd)
		return
	}

	versions := make([]string, 0)
	for _, file := range files {
		//log.Infof("fileName:%v", file.Name())
		if !strings.HasSuffix(file.Name(), ".md") {
			continue
		}
		version := strings.TrimSuffix(file.Name(), ".md")
		//log.Infof("file version:%v", version)
		versions = append(versions, version)
	}

	maxVersion := GetMaxVersion(versions)
	if maxVersion != "" {
		version = maxVersion
	}
	return
}

func GetMaxVersion(versions []string) string {
	maxVersion := ""
	for _, v := range versions {
		ret := semver.Compare(v, maxVersion)
		if ret > 0 {
			maxVersion = v
		}
	}
	return maxVersion
}
