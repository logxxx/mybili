package downloader

import (
	"context"
	"errors"
	"fmt"
	"github.com/logxxx/mybili_pkg/utils/fileutil"
	"github.com/logxxx/mybili_pkg/utils/log"
	"github.com/logxxx/mybili_pkg/utils/runutil"
	"io"
	"logxxx.com/mybili/modules/download/downloadproto"
)

func DownloadVideoDirectly(req *downloadproto.DownloadVideoReq) error {
	file, fileSize, err := fileutil.GetOrCreateFile(req.FileDir, req.FileName)
	if err != nil {
		log.Errorf("DownloadVideo getOrCreateFile err:%v req:%v", err, log.JSON(req))
		return err
	}
	defer file.Close()

	buf, err := GetDownloadBuf(req.Aid, req.Page, req.VideoUrl, fileSize)
	if err != nil {
		log.Errorf("DownloadVideo getDownloadBuf err:%v req:%v", err, log.JSON(req))
		return err
	}
	defer buf.Close()

	_, err = io.Copy(file, buf)
	if err != nil {
		log.Errorf("DownloadVideo io.Copy err:%v", err)
		return err
	}

	log.Infof("下载完成!")

	return nil
}

func DownloadVideo(req *downloadproto.DownloadVideoReq) (*downloadproto.DownloadVideoResp, error) {

	ctx, cancel := context.WithCancel(context.Background())
	runner := NewTaskRunner(ctx, req, cancel)

	err := GetTaskManager().AddRunner(runner)
	if err != nil {
		log.Errorf("DownloadVideo AddRunner err:%v req:%+v", err, log.JSON(req))
		return nil, err
	}

	runutil.RunSafe(func() {
		err := runner.Run(ctx)
		if err != nil {
			log.Errorf("TaskRunner.Run err:%v", err)
		}
	})

	resp := &downloadproto.DownloadVideoResp{
		TaskID: runner.TaskID,
	}

	return resp, nil

}

func StopDownload(taskID string) error {
	runner := GetTaskManager().GetRunner(taskID)
	if runner == nil {
		return fmt.Errorf("runner not found. task id:%v", taskID)
	}

	if runner.CloseFn != nil {
		runner.CloseFn()
	}

	return nil

}

func GetDownloadStatus(taskID string) (*downloadproto.DownloadState, error) {

	runner := GetTaskManager().GetRunner(taskID)
	if runner == nil {
		return nil, errors.New("runner not exists")
	}

	resp := &downloadproto.DownloadState{
		StartTime:  runner.State.StartTime,
		StopTime:   runner.State.StopTime,
		Speed:      runner.State.Speed,
		TotalSize:  runner.State.TotalSize,
		FinishSize: runner.State.FinishSize,
		ErrMsg:     runner.State.ErrMsg,
	}

	return resp, nil

}
