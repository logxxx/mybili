package danmu_test

import (
	"logxxx.com/mybili/modules/downloader/danmu"
	"testing"
)

func TestParseTimePoint(t *testing.T) {
	h, m, s := danmu.ParseTimePoint((3600 + 60 + 1) * 1000)
	t.Logf("h:%v m:%v s:%v", h, m, s)
}
