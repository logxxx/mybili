package danmu

import (
	"encoding/json"
	"fmt"
	"github.com/golang/protobuf/proto"
	"github.com/logxxx/mybili_pkg/utils/fileutil"
	"github.com/logxxx/mybili_pkg/utils/log"
	"io/ioutil"
	"logxxx.com/mybili/modules/download/downloadproto"
	"logxxx.com/mybili/modules/download/downloadutil"
	"logxxx.com/mybili/modules/downloader/danmu/danmuproto"
	"math/rand"
	"net/http"
	"path/filepath"
)

//https://api.bilibili.com/x/v2/dm/web/seg.so?type=1&oid=750976589&pid=770217424&segment_index=1
//https://api.bilibili.com/x/v1/dm/list.so?oid=%v

func DownloadDanmu(req *downloadproto.DownloadDanmuReq) error {

	round := 0
	soFileName := fmt.Sprintf("%v.so", req.FileName)

	for {

		round++

		url := fmt.Sprintf("https://api.bilibili.com/x/v2/dm/web/seg.so?type=1&oid=%v&pid=%v&segment_index=%v", req.Cid, req.Aid, round)
		code, resp, err := downloadutil.HttpGetRaw(url)
		if err != nil {
			log.Errorf("DownloadDanmu HttpGetRaw err:%v url:%v", err, url)
			return err
		}
		if code != http.StatusOK {
			if code == http.StatusNotModified { //没了
				break
			}
			return fmt.Errorf("invalid status:%v url:%v", code, url)
		}

		err = fileutil.WriteToFile(req.FileDir, soFileName, resp)
		if err != nil {
			log.Errorf("DownloadDanmu WriteToFile err:%v dir:%v filename:%v", err, req.FileDir, fmt.Sprintf("%v.so", req.FileName))
			return err
		}
	}

	assFileName := fmt.Sprintf("%v.ass", req.FileName)
	err := ConvertSegFileToAss(req.FileDir, soFileName, assFileName)
	if err != nil {
		log.Errorf("DownloadDanmu ConvertSegFileToAss err:%v dir:%v so:%v ass:%v", err, req.FileDir, soFileName, assFileName)
		return err
	}

	return nil
}

func ConvertSegFileToAss(fileDir, soFileName, assFileName string) error {

	positions := []float64{}

	height := 1080.0
	step := height / 70
	for {
		if height <= 0 {
			break
		}
		height -= step
		positions = append(positions, height)
	}

	file, _, err := fileutil.GetOrCreateFile(fileDir, assFileName)
	if err != nil {
		log.Errorf("ConvertSegFileToAss getOrCreateFile err:%v dir:%v name:%v", err, fileDir, assFileName)
		return err
	}
	defer file.Close()

	_, err = file.Write([]byte(getHeader()))
	if err != nil {
		log.Errorf("ConvertSegFileToAss Write header err:%v req:%v", err, getHeader())
		return err
	}

	data, err := ioutil.ReadFile(filepath.Join(fileDir, soFileName))
	if err != nil {
		return err
	}

	danmuResp := &danmuproto.DmSegMobileReply{}
	err = proto.Unmarshal(data, danmuResp)
	if err != nil {
		log.Errorf("DownloadVideo proto.Unmarshal err:%v", err)
		return err
	}

	danmuJson, _ := json.MarshalIndent(danmuResp, "", " ")
	fileutil.WriteToFile(fileDir, soFileName+".json", danmuJson)

	for i, row := range danmuResp.GetElems() {
		startH, startM, startS := ParseTimePoint(int64(row.Progress))
		startTime := fmt.Sprintf("%d:%02d:%02d.00", startH, startM, startS)
		endH, endM, endS := ParseTimePoint(int64(row.Progress + 30*1000))
		endTime := fmt.Sprintf("%d:%02d:%02d.00", endH, endM, endS)
		position := fmt.Sprintf(`{\pos(0,%v)}`, positions[i%len(positions)]) //TODO
		content := row.Content
		color := fmt.Sprintf(`{\cH%06x}`, row.Color)
		opacity := `{\alpha&H90}` //不透明度

		if row.Weight >= 10 { //TODO:固定位置弹幕

		}

		//Format: Layer, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text

		//Banner;<delay>;[<lefttoright>;[<range>]]---字幕横向移动,移动从屏幕边线出发到边线终止
		//delay---速度,表示移动一象素所用的时间,单位为0.001秒/1象素,delay=20表示1秒移动50象素的距离.由此可计算出头字幕从初始坐标到达终点坐标所用的时间为: (y1-y2的绝对值)*(delay/1000)(秒),delay越小移动速度越快.
		//lefttoright---0从右向左移动,1从左向右移动
		//range---左右的淡出淡入范围

		//delay 7~12差不多
		delay := 5 + rand.Intn(10)
		result := fmt.Sprintf("Dialogue: 0,%v,%v,*Default,NTP,0000,0000,0000,banner;%v;0;0,%v%v%v%v\n",
			startTime, endTime, delay, color, opacity, position, content)

		_, err = file.Write([]byte(result))
		if err != nil {
			log.Errorf("ConvertSegFileToAss Write header err:%v", err)
			return err
		}
	}

	return nil
}

func ParseTimePoint(input int64) (h, m, s int64) {
	s = input / 1000
	if s >= 60 {
		m = s / 60
		s = s % 60
	}
	if m >= 60 {
		h = m / 60
		m = m % 60
	}

	return
}

func ColorToRGB(color int) (red, green, blue int) {
	red = color >> 16
	green = (color & 0x00FF00) >> 8
	blue = color & 0x0000FF
	return
}
