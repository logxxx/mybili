package downloader

import "errors"

var (
	_mgr *TaskManager
)

type TaskManager struct {
	Runners []*TaskRunner
}

func init() {
	_mgr = NewTaskManager()
}

func NewTaskManager() *TaskManager {
	return &TaskManager{
		Runners: make([]*TaskRunner, 0),
	}
}

func GetTaskManager() *TaskManager {
	return _mgr
}

func (mgr *TaskManager) RemoveRunner(taskID string) {
	for i := range mgr.Runners {
		if mgr.Runners[i].TaskID == taskID {
			if i == len(mgr.Runners) {
				mgr.Runners = mgr.Runners[:i]
			} else {
				mgr.Runners = append(mgr.Runners[:i], mgr.Runners[i+1:]...)
			}
			break
		}
	}
}

func (mgr *TaskManager) AddRunner(add *TaskRunner) error {

	for _, r := range mgr.Runners {
		if r.Req.FileDir == add.Req.FileDir && r.Req.FileName == add.Req.FileName {
			return errors.New("duplicate runner")
		}
	}

	mgr.Runners = append(mgr.Runners, add)

	return nil
}

func (mgr *TaskManager) GetRunner(id string) *TaskRunner {

	for _, r := range mgr.Runners {
		if r.TaskID == id {
			return r
		}
	}
	return nil
}
