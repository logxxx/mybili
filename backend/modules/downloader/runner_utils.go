package downloader

import (
	"context"
	"fmt"
	"github.com/logxxx/mybili_pkg/utils/log"
	"net/http"
	"os"
	"path/filepath"
	"time"
)

func genCheckRedirectFunc(referer string) func(req *http.Request, via []*http.Request) error {
	return func(req *http.Request, via []*http.Request) error {
		req.Header.Set("Referer", referer)
		return nil
	}
}

func (r *TaskRunner) refresh(ctx context.Context, closeFns ...func()) {

	startTime := time.Now()

	totalSizeMB := float64(r.State.TotalSize) / 1024 / 1024
	round := 0
	for {
		select {
		case <-ctx.Done():
			log.Infof("refresh 收到了退出信号!")
			for _, fn := range closeFns {
				fn()
			}
			return
		case <-time.After(1 * time.Second):
			round++
			fileInfo, _ := os.Stat(filepath.Join(r.Req.FileDir, r.Req.FileName))
			fileSizeMB := float64(fileInfo.Size()) / 1024 / 1024

			now := time.Now()
			duration := now.Sub(startTime).Seconds()
			addSize := fileInfo.Size() - r.State.FinishSize
			speed := float64(addSize) / duration //b
			showSpeed := speed
			speedUnit := "b"
			if showSpeed > 1024 {
				showSpeed = showSpeed / 1024
				speedUnit = "kb"
			}
			if showSpeed > 1024 {
				showSpeed = showSpeed / 1024
				speedUnit = "mb"

			}
			showSpeedText := fmt.Sprintf("%.2f%v/s", showSpeed, speedUnit)
			log.Dumb("文件名:%v 已下载:%.2f/%.2fMB(%.2f%%) 速度:%v", fileInfo.Name(), fileSizeMB, totalSizeMB, fileSizeMB*100/totalSizeMB, showSpeedText)

			r.State.FinishSize = fileInfo.Size()
			r.State.Speed = int64(speed)

			if speed == 0 {
				r.State.ZeroSpeedCounter++
			} else {
				r.State.ZeroSpeedCounter = 0
			}

			startTime = now
		}
	}
}
