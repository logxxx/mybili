package downloader

import (
	"context"
	"fmt"
	"github.com/logxxx/mybili_pkg/consts"
	"github.com/logxxx/mybili_pkg/utils/fileutil"
	"github.com/logxxx/mybili_pkg/utils/log"
	"github.com/logxxx/mybili_pkg/utils/runutil"
	"io"
	"logxxx.com/mybili/modules/download/downloadconst"
	"logxxx.com/mybili/modules/download/downloadproto"
	"logxxx.com/mybili/modules/report"
	"net/http"
	"time"
)

type TaskRunner struct {
	TaskID  string
	Req     *downloadproto.DownloadVideoReq
	State   DownloadState
	Ctx     context.Context
	CloseFn func() `json:"-"`
}

type DownloadState struct {
	Speed            int64
	StartTime        int64
	StopTime         int64
	TotalSize        int64 //b
	FinishSize       int64 //b
	ErrMsg           string
	ZeroSpeedCounter int
}

func NewTaskRunner(ctx context.Context, req *downloadproto.DownloadVideoReq, closeFn func()) *TaskRunner {
	r := &TaskRunner{
		TaskID:  fmt.Sprintf("%v", time.Now().UnixNano()),
		Req:     req,
		Ctx:     ctx,
		CloseFn: closeFn,
	}

	return r

}

func GetDownloadBuf(aid int64, page int64, url string, doneSize int64) (io.ReadCloser, error) {

	//log.Infof("TaskRunner.getDownloadBuf doneSize:%v", doneSize)

	referer := fmt.Sprintf("%v?aid=%v", downloadconst.ApiGetCid, aid)
	for i := int64(1); i <= page; i++ {
		referer += fmt.Sprintf("/?p=%d", i)
	}

	//TODO:利用Content-RangeN段下载

	client := http.Client{CheckRedirect: genCheckRedirectFunc(referer)}

	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Errorf("getDownloadBuf NewRequest err:%v", err)
		return nil, err
	}
	request.Header.Set("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:56.0) Gecko/20100101 Firefox/56.0")
	request.Header.Set("Accept", "*/*")
	request.Header.Set("Accept-Language", "en-US,en;q=0.5")
	request.Header.Set("Accept-Encoding", "gzip, deflate, br")
	request.Header.Set("Range", fmt.Sprintf("bytes=%v-", doneSize))
	request.Header.Set("Referer", referer)
	request.Header.Set("Origin", "https://www.bilibili.com")
	request.Header.Set("Connection", "keep-alive")

	resp, err := client.Do(request)
	if err != nil {
		log.Errorf("DownloadVideo client.Do err:%v", err)
		return nil, err
	}

	/*
		if resp.StatusCode != http.StatusPartialContent {
			err := fmt.Errorf("invalid statusCode:%v", resp.StatusCode)
			log.Errorf("DownloadVideo client.Do err:%v", err)
			return nil, err
		}

	*/

	return resp.Body, nil
}

func (r *TaskRunner) Run(ctx context.Context) (err error) {

	r.State.StartTime = time.Now().Unix()
	r.State.TotalSize = r.Req.Size
	defer func() {
		r.State.StopTime = time.Now().Unix()
		if err != nil {
			r.State.ErrMsg = err.Error()
		}
	}()

	file, fileSize, err := fileutil.GetOrCreateFile(r.Req.FileDir, r.Req.FileName)
	if err != nil {
		log.Errorf("DownloadVideo getOrCreateFile err:%v req:%v", err, log.JSON(r.Req))
		return err
	}
	defer file.Close()

	buf, err := GetDownloadBuf(r.Req.Aid, r.Req.Page, r.Req.VideoUrl, fileSize)
	if err != nil {
		log.Errorf("DownloadVideo getDownloadBuf err:%v req:%v", err, log.JSON(r.Req))
		return err
	}
	defer buf.Close()

	runutil.RunSafe(func() {
		r.refresh(ctx,
			func() { file.Close() },
			func() { GetTaskManager().RemoveRunner(r.TaskID) }, //通知父管理器，移除自己
		)
	})

	_, err = io.Copy(file, buf)
	if err != nil {
		log.Errorf("DownloadVideo io.Copy err:%v", err)
		return err
	}

	log.Infof("下载完成!")

	runutil.RunSafe(func() {
		reportReq := &report.ReportReq{
			Action:  consts.ActionFinishTask,
			Payload: r.State,
		}
		err := report.Report(reportReq)
		if err != nil {
			log.Errorf("DownloadVideo Report err:%v", err)
		}
	})

	return
}
