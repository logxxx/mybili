package downloader

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/logxxx/mybili_pkg/utils/log"
)

func InitWeb() {
	g := gin.Default()
	g.GET("/runners", func(c *gin.Context) {
		runners, err := json.Marshal(GetTaskManager().Runners)
		if err != nil {
			log.Errorf("Marshal Runners err:%v", err)
			c.JSON(500, err)
			return
		}
		log.Infof("runners:%v", string(runners))
		c.String(200, string(runners))
	})
	err := g.Run(":8083")
	if err != nil {
		log.Errorf("downloader.InitWeb Run err:%v", err)
	}
}
